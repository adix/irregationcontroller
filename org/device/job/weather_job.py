from datetime import datetime

from org.device.channel.channel import QType
from org.device.json import json_converter as js
from org.device.json.object.trigger import Trigger, Cron
from org.device.services.weaterservice import CurrentWeather
from org.device.util import logger

__author__ = 'anzietek'


class WeatherJob(js.JsonMixin):
    _json_fields = [js.StrAttr('country_code'), js.StrAttr('api_key'), js.Attr('trigger', _type=Trigger),
                    js.BoolAttr('on')]

    def __init__(self, country_code, api_key, trigger):
        self.country_code = country_code
        self.api_key = api_key
        self.trigger = trigger
        self.on = True
        self._init()

    def _init(self):
        self.rain = lambda code: True if (code >= 300 and code <= 321) or (code >= 500 and code <= 531) else False
        self._weatherService = CurrentWeather(api_key=self.api_key)
        self._channel = None  # Channel()

    def weather_action(self):
        if not self.on:
            return

        response = self._take_weather()
        rain = False
        dt = datetime.now()
        if isinstance(response, dict):
            weathers = response.get('weather', None)
            dt = response.get('dt', None)
            if dt and weathers and isinstance(weathers, list):
                for weather in weathers:
                    if isinstance(weather, dict):
                        weather_code = weather.get('id', None)
                        if weather_code:
                            rain = self.rain(weather_code)
        if self._channel:
            ldt = datetime.fromtimestamp(dt) if isinstance(dt, int) else dt
            if ldt:
                logger.info('Pobrana pogoda o {}  jest deszczowa ? {}'.format(ldt.strftime("%Y-%m-%d %H:%M"), rain))
            self._channel.update((dt, rain),QType.WEATHER)

    @property
    def channel(self):
        return self._channel

    @channel.setter
    def channel(self, channel):
        self._channel = channel

    def _take_weather(self):
        pass


class WeatherJobByZipOrId(WeatherJob):
    _json_fields = [js.StrAttr('zip'), js.StrAttr('city_id')]

    def __init__(self, zip='33-100', country_code='pl', api_key='253efb9c2fca58135d84c1ed24be4d65',
                 trigger=Cron(minute=0, timezone='Europe/Warsaw'), city_id='757026'):
        super().__init__(country_code, api_key, trigger)
        self.zip = zip
        self.city_id = city_id

    def _take_weather(self):
        _id = None
        if self.city_id:
            try:
                _id = int(self.city_id)
            except:
                logger.info('Wrong city_id {}'.format(self.city_id))
        if _id:
            return self._weatherService.weater_by_id(_id)
        return self._weatherService.weather_by_zip(self.zip, self.country_code)
