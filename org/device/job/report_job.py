import datetime
import json
import pika
from pymongo import MongoClient
from org.device.channel.channel import QueueChannelMixin
from org.device.config import DB_NAME, DB_COLL_REPORTS
from org.device.json import json_converter as js
from org.device.json.object.trigger import Trigger, Interval
from org.device.util.logging import LoggerMixin

__author__ = 'anzietek'


class ReportJob(js.JsonMixin, QueueChannelMixin):
    _json_fields = [js.Attr('trigger', _type=Trigger)]

    def __init__(self, trigger=Interval(seconds=5)):
        self.__generators = {}
        self.__in = None
        self.trigger = trigger
        self.__init_generators()

    def __init_generators(self):
        self.__add_generator(ConsoleReport())
        self.__add_generator(MongoReport())
        self.__add_generator(OnlineWebReport())

    def channel_out(self):
        raise NotImplemented()

    def report_action(self):
        while True:
            item = self.get_nowait()
            if item:
                for rgen in self.generators.values():
                    if rgen.enabled:
                        rgen.generate(item)
            else:
                break

    def report_generator(self, rgen):
        if not isinstance(rgen, ReportGenerator):
            return False
        return self.__add_generator(rgen)

    def get_report_generator(self, name):
        return self.generators.get(name, None)

    @property
    def generators(self):
        return self.__generators

    def __add_generator(self, rgen):
        if isinstance(rgen, ReportGenerator):
            self.__generators[rgen.name] = rgen
            return True
        return False

    def __remove_generator(self, rgen):
        try:
            del self.__generators[rgen.name]
            return True
        except KeyError:
            return False


class ReportGenerator(js.JsonMixin, LoggerMixin):
    _json_fields = [js.StrAttr('name'), js.BoolAttr('enabled')]

    def __init__(self, name, enabled=False):
        self.name = name
        self.enabled = enabled

    def generate(self, item):
        pass


class ConsoleReport(ReportGenerator):
    _json_fields = []

    def __init__(self):
        super().__init__('console', False)

    def generate(self, item):
        print(item)


class MongoReport(ReportGenerator):
    _json_fields = [js.StrAttr('host'), js.IntAttr('port'), js.StrAttr('db_name'), js.StrAttr('coll_name')]

    def __init__(self, host='localhost', port=27017, db_name='db_orange', coll_name='reports'):
        super().__init__('mongo', True)
        self.host = host
        self.port = port
        self.db_name = DB_NAME  # db_name
        self.coll_name = DB_COLL_REPORTS  # coll_name
        self._init()

    def _init(self):
        self.client = MongoClient(self.host, self.port)
        self.db = self.client[self.db_name]
        self.reports = self.db[self.coll_name]

    def generate(self, item):
        if isinstance(item, tuple):
            return
        with self.client:
            try:
                data = json.loads(item)
                data['date'] = datetime.datetime.now()
                self.reports.insert_one(data)
            except Exception as e:
                self.logger.error(e)


class OnlineWebReport(ReportGenerator):
    _json_fields = [js.StrAttr('host'), js.IntAttr('port'), js.StrAttr('queue_name'),
                    js.StrAttr('user'), js.StrAttr('password')]

    def __init__(self, host='localhost', port=5672, queue_name='queue', user='', password=''):
        super().__init__('online', False)
        self.host = host
        self.port = port
        self.queue_name = queue_name
        self.user = user
        self.password = password

    def _init(self):
        self.credentials = None
        if self.user and self.password:
            self.credentials = pika.PlainCredentials(self.user, self.password)
        self.parameters = pika.ConnectionParameters(self.host, self.port, credentials=self.credentials)

    def generate(self, item):
        if isinstance(item, tuple):
            return
        with pika.BlockingConnection(parameters=self.parameters if self.parameters else None) \
                as connection:
            channel = connection.channel()
            channel.exchange_declare(exchange='direct', exchange_type='direct')
            channel.queue_declare(queue=self.queue_name)
            try:
                channel.basic_publish(exchange='direct',
                                      routing_key='/report',
                                      body=item)
            except Exception as e:
                self.logger.info('Exception {} for {}'.format(e, item))

                # connection.close()

#
# class DummyReportJob(ReportJob):
#     def __init__(self, trigger=Interval(seconds=5)):
#         super().__init__(trigger)
#
#     def report_action(self):
#         while True:
#             msg = self.get_nowait()
#             if msg:
#                 print(msg)
#             else:
#                 break
