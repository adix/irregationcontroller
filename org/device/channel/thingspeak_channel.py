from concurrent.futures import ThreadPoolExecutor
from multiprocessing.connection import Client

from multiprocessing import Queue

from org.device.json.object.thingspeak_item import ThingSpeakItem
from org.device.util import logger


class ThingSpeakChannelMixin:
    tsQueue = Queue()
    def ts_put_nowait(self, obj):
        self.tsQueue.put_nowait(ThingSpeakItem.object2json(obj))
        with ThreadPoolExecutor(max_workers=1) as e:
            e.submit(self.send)

    def send(self):
        try:
            tsConnection = Client(('localhost',25000), authkey=b'auth')
            tsConnection.send(self.tsQueue.get_nowait())
        except Exception as e:
            logger.exception(e)