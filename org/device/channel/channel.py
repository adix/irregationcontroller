import threading
from collections import namedtuple
from enum import Enum, unique
from multiprocessing import Queue
from queue import Full, Empty
from weakref import WeakValueDictionary
from org.device.channel.nullqueue import NullQueue
from org.device.util.logging import logger

__author__ = 'anzietek'


class QueueChannelMixin:
    __out = NullQueue()
    __in = NullQueue()

    @property
    def channel_in(self):
        return self.__in

    @channel_in.setter
    def channel_in(self, _in):
        if not _in:
            self.__in = NullQueue()
        elif isinstance(_in, type(Queue())) or isinstance(_in, type(NullQueue())):
            self.__in = _in
        else:
            logger.info('In %s must be %s' % (_in, type(Queue())))
            raise TypeError('%s must be %s' % (_in, type(Queue())))

    @property
    def channel_out(self):
        return self.__out

    @channel_out.setter
    def channel_out(self, _out):
        if not _out:
            self.__out = NullQueue()
        elif isinstance(_out, type(Queue())) or isinstance(_out, type(NullQueue())):
            self.__out = _out
        else:
            logger.info('TypeError Out %s must be %s' % (_out, type(Queue())))
            raise TypeError('%s must be %s' % (_out, type(Queue())))

    def put_nowait(self, obj):
        try:
            self.__out.put_nowait(obj)
            return True
        except Full as e:
            # logger.debug('Out %s is full' % self.__out)
            return False

    def get_nowait(self):
        try:

            return self.__in.get_nowait()
        except Empty as e:
            # logger.debug('In %s is empty' % self.__in)
            return None


@unique
class QType(Enum):
    REPORT = 1
    WEATHER = 2


class Channel:
    Key = namedtuple('Key', 'name qtype')

    def __init__(self):
        self.__queues = WeakValueDictionary()
        self.__lock = threading.RLock()

    def register_queue(self, name, qtype):
        with self.__lock:
            key=self.Key(name,qtype)
            queue = self.__queues.get(key, None)
            if not queue:
                queue = Queue()
                self.__queues[key] = queue
            return queue

    def update(self, value, qtype):
        with self.__lock:
            keys = filter(lambda key: key.qtype == qtype, self.__queues.keys())
            for k in keys:
                self.__queues[k].put_nowait(value)
            # for queue in self.__queues.values():
            #     queue.put_nowait(value)
