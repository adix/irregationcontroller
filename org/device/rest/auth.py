from functools import wraps

from flask import request


# api.add_resource(TokenListResource, '/tokens')
#
# class TokenListResource(Resource):
#    parser = reqparse.RequestParser()
#    parser.add_argument('client_id', type=int, required=True, length=2)
#    parser.add_argument('client_secret_hash', type=str, required=True)
#
#
#      def post(self):
#        data = self.parser.parse_args()
#        client_id = data.get('client_id', '')
#        client_secret_hash = data.get('client_secret_hash', '')
#        is_client = Client() \
#            .verify_client(id=client_id,  client_secret_hash=client_secret_hash)
# #verify same way we did in client side, client id gives us client_secret from database then we
# # do same algo that we did client side then match that hash with client_secret_hash
#        if is_client:
#            Token().create(client_id, expires_on=expires_on)
#            client_obj = Client().get(client_id)
#            token = client_obj.token
#            data = token
#            return resp_envelope(status=STATUS_SUCCESS,
#                                 data=data), httplib.CREATED
#        else:
#            return cannot_authenticate()


#Prosta metoda autoryzacji
from org.device.config import API_KEY


def authorize(api_key):
    if API_KEY == api_key:
        return True
    return False


def requires_api_key(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        api_key = request.headers.get('x-api-key', None)
        if not api_key:
            return missing_headers(['x-api-key'])
        elif not authorize(api_key):  # check with database
            return cannot_authenticate()
        return f(*args, **kwargs)

    return decorated


def missing_headers(missing_headers):
    missing_header = ','.join(missing_headers)

    message = {
        'status': 'error',
        'status_code': 400,
        'error_message': 'Missing header',
        'error_description': missing_header
    }
    return message, 400


def cannot_authenticate():
    message = {
        'status': 'error',
        'status_code': 401,
        'error_message': 'Not Authorized'
    }
    return message, 401

    # def access_denied():
    #    message = resp_envelope(status=0, data={}, err_code=1010,
    #                            err_mesg="Access denied", err_desc='access_denied')
    #    return message, BAD_REQUEST
