import json
import sys
import traceback

from flask import Flask, Blueprint
from flask.ext.cors import CORS
from flask.ext.restplus import Api, Resource

from org.device.config import HOST, PORT, DEBUG, THREADED, SSL_CONTEXT, ORANGEPI_ZERO, POMP
from org.device.rest import auth
from org.device.rest.executor import Executor, Job, ArgsWrap
from org.device.util import logger
from org.device.util.orangepi_zero import setup

__author__ = 'anzietek'
try:
    from flask import _app_ctx_stack as stack
    from OPi.pin_mappings import sunxi
    from OPi import GPIO as gpio
except ImportError:
    from flask import _request_ctx_stack as stack

DEVICE = 'orangepi'

app = Flask(__name__)
CORS(app)

api_v1 = Blueprint('api', __name__, url_prefix='/' + DEVICE)

api = Api(api_v1, version='1.0', title='Relays Controller API',
          description='',
          )

ns = api.namespace('api/1.0', description='')

if ORANGEPI_ZERO:
    setup()

executor = Executor()


@ns.route('/tasks')
class Tasks(Resource):
    @auth.requires_api_key
    def get(self):
        return {'jobs': executor.results()}


@ns.route('/scheduler/start')
class Start(Resource):
    @auth.requires_api_key
    def get(self):
        result = executor.start()
        return {'job_id': result}, 200


@ns.route('/scheduler/shutdown')
class Shutdown(Resource):
    @auth.requires_api_key
    def get(self):
        result = executor.shutdown()
        return {'job_id': result}, 200


@ns.route('/scheduler/pause')
class Pause(Resource):
    @auth.requires_api_key
    def get(self):
        job = Job('AppScheduler', 'pause_all')
        return {'job_id': executor.submit_job(job)}, 200


@ns.route('/scheduler/resume')
class Resume(Resource):
    @auth.requires_api_key
    def get(self):
        job = Job('AppScheduler', 'resume_all')
        return {'job_id': executor.submit_job(job)}, 200


parser = api.parser()
parser.add_argument('group', type=dict)
parser.add_argument('groups', type=str)
parser.add_argument('weather', type=dict)
parser.add_argument('report', type=dict)
parser.add_argument('report_trigger', type=dict)
parser.add_argument('console', type=dict)
parser.add_argument('mongo', type=dict)
parser.add_argument('online', type=dict)
parser.add_argument('db_groups', type=str)
parser.add_argument('pomp_state', type=bool)
parser.add_argument('moisture', type=dict)
parser.add_argument('relay', type=dict)


@ns.route('/scheduler/modify', endpoint='modify')
class ModifyScheduler(Resource):
    @auth.requires_api_key
    def post(self):
        args = parser.parse_args()
        results = []

        group = args['group'] or args['groups']  # else args['groups']
        if group:
            job = Job('AppScheduler', 'modify_group', group)
            result = executor.submit_job(job)
            results.append(result)

        weather = args['weather']
        if weather:
            job = Job('AppScheduler', 'weather_task', weather)
            result = executor.submit_job(job)
            results.append(result)
            job = Job('MongoDb', 'save_weathersetting', ArgsWrap(weather))
            result = executor.submit_job(job)
            results.append(result)
        report = args['report']
        if report:
            job = Job('AppScheduler', 'report_job', report)
            result = executor.submit_job(job)
            results.append(result)
        moisture = args['moisture']
        if moisture:
            # zapisac stan czujnika do bazy
            # moisture = json.loads(moisture)
            job = Job('AppScheduler', 'moisture_sensor', moisture)
            result = executor.submit_job(job)
            results.append(result)
            job = Job('MongoDb', 'save_sensorsetting', ArgsWrap(moisture))
            result = executor.submit_job(job)
            results.append(result)

        if len(results) > 1:
            return {'job_ids': results}, 201
        elif len(results) == 1:
            return {'job_id': results[0]}, 201
        else:
            return {'job_id': None}, 201


@ns.route('/report/modify', endpoint='reportmodify')
class ModifyReport(Resource):
    @auth.requires_api_key
    def post(self):
        args = parser.parse_args()
        results = []
        report_trigger = args['report_trigger']
        if report_trigger:
            job = Job('AppScheduler', 'report_job_trigger', report_trigger)
            result = executor.submit_job(job)
            results.append(result)

            job = Job('MongoDb', 'save_reportsetting', ArgsWrap(('trigger', report_trigger)))
            result = executor.submit_job(job)
            results.append(result)

        console = args['console']
        if console:
            job = Job('AppScheduler', 'report_generator', console)
            result = executor.submit_job(job)
            results.append(result)
        mongo = args['mongo']
        if mongo:
            job = Job('AppScheduler', 'report_generator', mongo)
            result = executor.submit_job(job)
            results.append(result)
            job = Job('MongoDb', 'save_reportsetting', ArgsWrap(('mongo', mongo)))
            result = executor.submit_job(job)
            results.append(result)

        online = args['online']
        if online:
            job = Job('AppScheduler', 'report_generator', online)
            result = executor.submit_job(job)
            results.append(result)
            job = Job('MongoDb', 'save_reportsetting', ArgsWrap(('online', online)))
            result = executor.submit_job(job)
            results.append(result)

        if len(results) > 1:
            return {'job_ids': results}, 201
        elif len(results) == 1:
            return {'job_id': results[0]}, 201
        else:
            return {'job_id': None}, 201


@ns.route('/manual/modify', endpoint='manual')
class ManualControl(Resource):
    @auth.requires_api_key
    def post(self):
        args = parser.parse_args()
        relay = args['relay']
        if relay:
            job = Job('ManualDriver', 'execute', relay)
            result = executor.execute(job)
            return {'execute': result}, 201


@ns.route('/scheduler/show/<id>', endpoint='show')
class ShowScheduler(Resource):
    @auth.requires_api_key
    def get(self, id):
        if id == 'groups':
            job = Job('AppScheduler', 'groups')
            result = executor.execute(job)
            return executor.serialize(result), 201
        elif id == 'weather':
            job = Job('AppScheduler', 'weather_job')
            result = executor.execute(job)
            return executor.serialize(result), 201
        elif id == 'report':
            job = Job('AppScheduler', 'report_job')
            result = executor.execute(job)
            return executor.serialize(result), 201
        elif id == 'moisture':
            job = Job('AppScheduler', 'get_moisture_sensor')
            result = executor.execute(job)
            return executor.serialize(result), 201
        elif id == 'status':
            job = Job('AppScheduler', 'running')
            result = executor.execute(job)
            return {'running': result}, 201


@ns.route('/mongodb/save', endpoint='mongo_save')
class MongoDbSave(Resource):
    @auth.requires_api_key
    def post(self):
        args = parser.parse_args()
        db_groups = args['db_groups']
        db_groups = json.loads(db_groups)
        job = Job('MongoDb', 'save_groups', db_groups)
        result = executor.submit_job(job)
        return executor.serialize(result), 201


@ns.route('/mongodb/load/<id>', endpoint='mongo_load')
class MongoDbLoad(Resource):
    @auth.requires_api_key
    def get(self, id):
        result = dict([('groups', None), ('exception', None), ('reports', None)])
        if id == 'groups':
            job = Job('MongoDb', 'load_groups')
        if id == 'reports':
            job = Job('MongoDb', 'load_reports')
        try:
            result[id] = executor.execute(job)[id]
        except Exception as e:
            logger.info(e)
            result['exception'] = self._exc2json(e)
        return result, 201

    def _exc2json(self, exc):
        try:
            return json.dumps(exc)
        except Exception as e:
            return exc.__class__.__name__


@ns.route('/report/show/<id>', endpoint='reportshow')
class ShowReport(Resource):
    @auth.requires_api_key
    def get(self, id):
        if id == 'trigger':
            job = Job('AppScheduler', 'get_report_job_trigger')
            result = executor.execute(job)
            return executor.serialize(result), 201
        if id in ('console', 'mongo', 'online'):
            job = Job('AppScheduler', 'get_report_generator', id)
            result = executor.execute(job)
            return executor.serialize(result), 201


@ns.route('/pomp/show', endpoint='pompshow')
class ShowPomp(Resource):
    @auth.requires_api_key
    def get(self):
        return {'pomp_state': POMP.ENABLED}, 201


@ns.route('/pomp/modify', endpoint='pompmodify')
class ModifyPomp(Resource):
    def post(self):
        # global POMP_ENABLE
        args = parser.parse_args()
        state = args['pomp_state']
        if isinstance(state, bool):
            POMP.ENABLED = state
            job = Job('MongoDb', 'save_pumpsetting', ArgsWrap({'enabled': state}))
            executor.submit_job(job)

        return {'pomp_state': POMP.ENABLED}, 201


def handle_exception(exc_type, exc_value, exc_traceback):
    logger.info("".join(traceback.format_exception(exc_type, exc_value, exc_traceback)))
    sys.exit(1)


def run_default_config():
    executor.start()
    job = Job('MongoDb', 'load_groups')
    sensor_job = Job('MongoDb', 'load_sensorsetting')
    pump_job = Job('MongoDb', 'load_pumpsetting')
    weather_job = Job('MongoDb', 'load_weathersetting')

    try:
        group = executor.execute(job)['groups']
        group = json.dumps(group)
        job = Job('AppScheduler', 'modify_group', group)
        executor.submit_job(job)
    except Exception as e:
        logger.info(e)
    try:
        sensor = executor.execute(sensor_job)['sensor']
        job = Job('AppScheduler', 'moisture_sensor', sensor)
        executor.submit_job(job)
    except Exception as e:
        logger.info(e)
    try:
        pump = executor.execute(pump_job)['pump']
        POMP.ENABLED = pump['enabled']
    except Exception as e:
        logger.info(e)
    try:
        weather = executor.execute(weather_job)['weather']
        job = Job('AppScheduler', 'weather_task', weather)
        executor.submit_job(job)
    except Exception as e:
        logger.info(e)

    try:
        trigger_job = Job('MongoDb', 'load_reportsetting', ArgsWrap('trigger'))
        trigger = executor.execute(trigger_job)['trigger']
        job = Job('AppScheduler', 'report_job_trigger', trigger)
        executor.submit_job(job)
    except Exception as e:
        logger.info(e)

    try:
        mongo_job = Job('MongoDb', 'load_reportsetting', ArgsWrap('mongo'))
        mongo = executor.execute(mongo_job)['mongo']
        job = Job('AppScheduler', 'report_generator', mongo)
        executor.submit_job(job)
    except Exception as e:
        logger.info(e)
    try:
        online_job = Job('MongoDb', 'load_reportsetting', ArgsWrap('online'))
        online = executor.execute(online_job)['online']
        job = Job('AppScheduler', 'report_generator', online)
        executor.submit_job(job)
    except Exception as e:
        logger.info(e)


def main():
    sys.excepthook = handle_exception
    run_default_config()
    app.register_blueprint(api_v1)
    try:
        # ctx=SSL.Context(SSL.SSLv23_METHOD)
        # ctx.use_privatekey_file('key.pem')
        # ctx.use_certificate_file('cert.pem')
        # ctx = ('cert.pem', 'key.pem')
        app.run(host=HOST, port=PORT, debug=DEBUG, ssl_context=SSL_CONTEXT, threaded=THREADED, use_reloader=False)

    except Exception as e:
        logger.info(e)


if __name__ == '__main__':
    main()
