import inspect
import json
import uuid
from _collections_abc import Mapping
from collections import OrderedDict, namedtuple
from multiprocessing import Queue
from threading import Lock, Thread
from time import time
from apscheduler.schedulers.background import BackgroundScheduler
from org.device.config import DEFAULT_GROUP_COUNT
from org.device.core.manual_driver import ManualDriver
from org.device.core.scheduler import AppScheduler
from org.device.job.report_job import ConsoleReport, MongoReport, OnlineWebReport
from org.device.job.weather_job import WeatherJobByZipOrId
from org.device.json.object.group import Group
from org.device.json.object.moisture_sensor import MoistureSensor
from org.device.json.object.relay_dto import RelayDTO
from org.device.json.object.timedelay import TimeDelay
from org.device.json.object.timedelta import TimeDeltaWrapper
from org.device.json.object.trigger import Interval, Cron
from org.device.services.mongoservice import MongoDb

__author__ = 'anzietek'


def namedtuple_with_defaults(typename, field_names, default_values=[]):
    T = namedtuple(typename, field_names)
    T.__new__.__defaults__ = (None,) * len(T._fields)
    if isinstance(default_values, Mapping):
        prototype = T(**default_values)
    else:
        prototype = T(*default_values)
    T.__new__.__defaults__ = tuple(prototype)
    return T


Job = namedtuple_with_defaults('Job', ['clsname', 'action', 'args', 'callback', 'cargs'])


class JobResult:
    def __init__(self, job_id, clsname, action):
        self._job_id = job_id
        self._result = {}
        self._result['id'] = job_id
        self._result['name'] = clsname
        self._result['action'] = action
        self._result['result'] = None
        self._result['exception'] = None
        self._result['status'] = 'running'  # done
        self._result['timestamp'] = time()

    def done(self):
        self._result['status'] = 'done'

    @property
    def job_id(self):
        return self._job_id

    @property
    def job_result(self):
        return self._result  # deepcopy(self._result)

    @property
    def result(self):
        return self._result['result']

    @result.setter
    def result(self, value):
        self._result['result'] = value
        # self.done()

    @property
    def exception(self):
        return self._result['exception']

    @exception.setter
    def exception(self, value):
        self._result['exception'] = value
        # self.done()


class ArgsWrap:
    def __init__(self, args):
        self._args = args

    @property
    def args(self):
        return self._args


class ClassSerializer:
    class Dummy:
        def json2object(self, json):
            return json

    def __init__(self):
        self.serializers = {'__Dummy__': self.Dummy, 'Group'
        : Group, 'Interval': Interval, 'Cron': Cron, 'ConsoleReport': ConsoleReport, 'MongoReport': MongoReport,
                            'OnlineWebReport': OnlineWebReport, 'WeatherJobByZipOrId': WeatherJobByZipOrId,
                            'MoistureSensor': MoistureSensor, 'RelayDTO': RelayDTO}

    def serialize(self, obj):

        if hasattr(obj, 'object2json'):
            return obj.object2json(as_str=False)
        if isinstance(obj, list):
            result = []
            for o in obj:
                if hasattr(o, 'object2json'):
                    json = o.object2json(as_str=False)
                    result.append(json)
                else:
                    result.append(o)
            return result
        return obj

    def deserialize(self, obj):
        if isinstance(obj, dict):
            name = obj.get('__class__', None)  # '__Dummy__'
            deserializer = self.serializers.get(name, '__Dummy__')
            return deserializer.json2object(obj)
        if isinstance(obj, ArgsWrap):
            return obj.args
        return obj


# class SingletonMixin:
#     __singleton_lock = threading.Lock()
#     __singleton_instance = None
#
#     @classmethod
#     def instance(cls):
#         if not cls.__singleton_instance:
#             with cls.__singleton_lock:
#                 if not cls.__singleton_instance:
#                     cls.__singleton_instance = cls()
#         return cls.__singleton_instance


class Executor(ClassSerializer):
    def __init__(self, size=50):
        super().__init__()
        self._sanit = object()
        self._queue = Queue()
        # self.results = []
        self._results = OrderedDict()
        self._results_lock = Lock()
        self._executor_lock = Lock()
        self._executor = None
        self._running = False
        self._size = size
        self._objs = {}
        self._init_db()
        self._init_sched()
        self._init_mdriver()
        # self._deserializers = {'Group': Group.json2object()}

    def _init_sched(self):
        self._sched = AppScheduler(scheduler=BackgroundScheduler(), groups=self.init_groups)
        self._objs['AppScheduler'] = self._sched

    def _init_db(self):
        self._objs['MongoDb'] = MongoDb()

    def _init_mdriver(self):
        self._objs['ManualDriver'] = ManualDriver()

    def _job_id(self):
        return str(uuid.uuid1())

    def _reinit_scheduler(self):

        db = self._objs['MongoDb']
        groups = self.init_groups
        try:
            dbgroups = db.load_groups()['groups']
            _groups = []
            for group in dbgroups:
                _groups.append(self.deserialize(group))
            if len(_groups):
                groups = _groups

        except Exception as e:
            pass
        self._sched = AppScheduler(scheduler=BackgroundScheduler(), groups=groups)
        self._objs['AppScheduler'] = self._sched
        try:
            sensor = db.load_sensorsetting()['sensor']
            sensor = self.deserialize(sensor)
            self._sched.moisture_sensor(sensor)
        except Exception as e:
            pass
        try:
            weather = db.load_weathersetting()['weather']
            weather = self.deserialize(weather)
            self._sched.weather_task(weather)
        except Exception as e:
            pass
        try:
            trigger = db.load_reportsetting('trigger')['trigger']
            trigger = self.deserialize(trigger)
            self._sched.report_job_trigger(trigger)
        except Exception as e:
            pass
        try:
            mongo = db.load_reportsetting('mongo')['mongo']
            mongo = self.deserialize(mongo)
            self._sched.report_generator(mongo)
        except Exception as e:
            pass
        try:
            online = db.load_reportsetting('online')['online']
            online = self.deserialize(online)
            self._sched.report_generator(online)
        except Exception as e:
            pass

    @property
    def init_groups(self):
        groups = []
        for id in range(0, DEFAULT_GROUP_COUNT):
            # group = Group.auto_execution(id=id, trigger=Interval(seconds=20, timezone='Europe/Warsaw'), action='off_on',
            #                              type='inclusive',
            #                              delay=TimeDelay(seconds=15), weather_td=TimeDeltaWrapper(minutes=5))
            group = Group.manual_execution(id=id, action='off', type='inclusive', delay=TimeDelay(seconds=15),
                                           report_on=False)
            groups.append(group)
        return groups

    def _execute(self):
        while 1:
            job_id, job = self._queue.get()  # clsname, action, args, callback, cargs
            result = JobResult(job_id, job.clsname, job.action)
            with self._results_lock:
                if len(self._results) >= self._size:
                    self._results.popitem(last=False)
                self._results.update({job_id: result.job_result})

            try:
                result.result = self.execute(job)
            except Exception as exc:
                # logger.info(exc)
                # logger.info(job.args)
                result.exception = self._exc2json(exc)
            if job.action == 'shutdown':
                self._reinit_scheduler()
            result.done()

        with self._executor_lock:
            self._running = False

    def results(self):
        with self._results_lock:
            return self._results

    def execute(self, job):
        if not isinstance(job, Job):
            raise TypeError('%s is not %s type' % (job, Job))
        obj = self._objs.get(job.clsname, None)
        if obj:
            attr = getattr(obj, job.action)
            if attr and callable(attr):
                sig = inspect.signature(attr)
                if not len(sig.parameters):
                    return attr()
                else:
                    return attr(self.deserialize(job.args))
            return attr

    def start(self):
        return self.submit_job(Job('AppScheduler', 'start'))

    def shutdown(self):
        return self.submit_job(Job('AppScheduler', 'shutdown'))

    def submit_job(self, job):  # (self, clsname, action, args=None, callback=None, cargs=None):
        if not isinstance(job, Job):
            raise TypeError('%s is not %s type' % (job, Job))
        with self._executor_lock:
            if not self._running:
                self._executor = Thread(target=self._execute)
                self._executor.daemon = True
                self._executor.start()
                self._running = True
        job_id = self._job_id()
        # self._queue.put_nowait((task_id, clsname, action, args, callback, cargs))
        self._queue.put_nowait((job_id, job))
        return job_id

    def _exc2json(self, exc):
        try:
            return json.dumps(exc)
        except Exception as e:
            return exc.__class__.__name__
