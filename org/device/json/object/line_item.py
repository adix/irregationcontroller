from datetime import datetime

from org.device.json import json_converter as js
from org.device.json.object.timedelay import TimeDelay

__author__ = 'anzietek'


class LineLogItem(js.JsonMixin):
    _json_fields = [js.StrAttr('name'), js.StrAttr('group_name'), js.StrAttr('action'), js.StrAttr('date_on'),
                    js.StrAttr('date_off'),
                    js.BoolAttr('rain'), js.IntListAttr('lines_on', _type=list),
                    js.IntListAttr('lines_off', _type=list), js.Attr('time_delay', TimeDelay),
                    js.BoolAttr('sensor'), js.StrAttr('mode'), js.BoolAttr('moisture')]

    def __init__(self, name, group_name, action, time_delay, rain, sensor=False, mode='NO', moisture=False):
        self.name = name
        self.group_name = group_name
        self.action = action
        self.time_delay = time_delay
        self.rain = rain
        self.sensor = sensor
        self.mode = mode
        self.moisture = moisture
        #self.
    def now(self):
        return datetime.now().strftime("%Y-%m-%d %H:%M:%S")