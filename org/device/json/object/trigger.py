import uuid
from apscheduler.triggers.cron import CronTrigger
from apscheduler.triggers.interval import IntervalTrigger
from apscheduler.util import timedelta_seconds
from org.device.json import json_converter as js

__author__ = 'anzietek'


class Trigger(js.JsonMixin):
    _json_fields = [js.StrAttr('trigger_id'), js.StrAttr('start_date'), js.StrAttr('end_date'), js.StrAttr('timezone')]

    def __init__(self, start_date=None, end_date=None, timezone=None):
        self.start_date = start_date
        self.end_date = end_date
        self.timezone = timezone
        self.trigger_id = str(uuid.uuid1())

    @classmethod
    def _get_all_json_fields(cls):
        return super()._get_all_json_fields()

    @property
    def name(self):
        pass


class IntervalTriggerWithRepeat(IntervalTrigger):
    def __init__(self, repeat=0, weeks=0, days=0, hours=0, minutes=0, seconds=0, start_date=None, end_date=None,
                 timezone=None):
        if not isinstance(repeat, int):
            raise TypeError('Param repeat must be int type')
        if repeat < 0:
            raise ValueError('Param repeat must be >= 0')

        self.repeat = repeat
        super().__init__(weeks, days, hours, minutes, seconds, start_date, end_date, timezone)
        self.count = repeat
        self.fire_time = None

    def get_next_fire_time(self, previous_fire_time, now):
        if not self.repeat:
            return super().get_next_fire_time(previous_fire_time, now)
        elif self.count:
            tds = 0
            if not self.fire_time:
                self.fire_time = super().get_next_fire_time(previous_fire_time, now)
            if self.fire_time:
                tds = timedelta_seconds(now - self.fire_time)
            if tds > 0:
                self.count -= 1
                self.fire_time = super().get_next_fire_time(previous_fire_time, now)
                return self.fire_time
            else:
                return super().get_next_fire_time(previous_fire_time, now)

        return None


class Interval(Trigger):
    '''
    Klasa opakowujaca IntervalTrigger
    '''
    _json_fields = [js.IntAttr('repeat'), js.IntAttr('weeks'), js.IntAttr('days'), js.IntAttr('hours'),
                    js.IntAttr('minutes'),
                    js.IntAttr('seconds')]

    def __init__(self, repeat=0, weeks=0, days=0, hours=0, minutes=0, seconds=0, start_date=None, end_date=None,
                 timezone=None):
        super().__init__(start_date, end_date, timezone)
        self.repeat = repeat
        self.weeks = weeks
        self.days = days
        self.minutes = minutes
        self.seconds = seconds
        self.hours = hours
        self._init()
        # self._interval = IntervalTrigger(weeks, days, hours, minutes,
        # seconds, start_date, end_date, timezone)

    def _init(self):
        if self.repeat is None:
            self.repeat = 0
        self._interval = IntervalTriggerWithRepeat(self.repeat, self.weeks, self.days, self.hours, self.minutes,
                                                   self.seconds, self.start_date, self.end_date, self.timezone)

    @property
    def name(self):
        # if not hasattr(self,'_interval'):
        # self._interval=IntervalTrigger(self.weeks, self.days, self.hours, self.minutes,
        # self.seconds, self.start_date, self.end_date, self.timezone)

        return self._interval


class Cron(Trigger):
    '''
    Klasa opakowujaca CronTrigger
    '''
    _json_fields = [js.IntAttr('year'), js.IntAttr('month'), js.IntAttr('day'), js.IntAttr('week'),
                    js.IntListAttr('day_of_week'), js.StrAttr('hour'), js.IntAttr('minute'), js.IntAttr('second'),
                    js.StrAttr('timezone')]

    def __init__(self, year=None, month=None, day=None, week=None, day_of_week=None, hour=None, minute=None,
                 second=None, start_date=None, end_date=None, timezone=None):
        super().__init__(start_date, end_date, timezone)
        self.year = year
        self.month = month
        self.day = day
        self.week = week
        self.day_of_week = day_of_week
        self.hour = hour
        self.minute = minute
        self.second = second
        self._init()
        # self._cron = CronTrigger(year, month, day, week, day_of_week, hour, minute,
        # second, start_date, end_date, timezone)

    def _init(self):
        dfw = None
        if isinstance(self.day_of_week, list):
            if len(self.day_of_week):
                dfw = ','.join(map(str, self.day_of_week))
        self._cron = CronTrigger(self.year, self.month, self.day, self.week, dfw, self.hour,
                                 self.minute, self.second, self.start_date, self.end_date, self.timezone)

    @property
    def name(self):
        # if not hasattr(self,'_cron'):
        # self._cron = CronTrigger(self.year, self.month, self.day, self.week, self.day_of_week, self.hour,
        # self.minute,self.second, self.start_date, self.end_date, self.timezone)
        return self._cron
