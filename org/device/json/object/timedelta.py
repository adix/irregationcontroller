import uuid
from datetime import timedelta

from org.device.json import json_converter as js


__author__ = 'anzietek'


class TimeDeltaWrapper(js.JsonMixin):
    _json_fields = [js.StrAttr('id'),js.IntAttr('days'), js.IntAttr('seconds'), js.IntAttr('microseconds'),
                    js.IntAttr('milliseconds'), js.IntAttr('minutes'), js.IntAttr('hours'), js.IntAttr('weeks')]


    def __init__(self, days=0, seconds=0, microseconds=0,
                 milliseconds=0, minutes=0, hours=0, weeks=0, ):
        self.days = days
        self.seconds = seconds
        self.microseconds = microseconds
        self.milliseconds = milliseconds
        self.minutes = minutes
        self.hours = hours
        self.weeks = weeks
        self.id = str(uuid.uuid1())
        self._init()

    def _init(self):
        self._timedelta = timedelta(self.days, self.seconds, self.microseconds, self.milliseconds, self.minutes,
                                    self.hours, self.weeks)


    @property
    def timedelta(self):
        return self._timedelta