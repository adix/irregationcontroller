#import js as js
from org.device.json import json_converter as js


class ThingSpeakItem(js.JsonMixin):
    _json_fields = [js.StrAttr('line_id'), js.StrAttr('state')]

    def __init__(self, line_id, state):
        self.line_id = line_id
        self.state = state
