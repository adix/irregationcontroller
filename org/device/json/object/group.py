from org.device.json import json_converter as js
from org.device.json.object.mode import Mode, ManualMode, AutoMode
from org.device.json.object.timedelay import TimeDelay
from org.device.json.object.timedelta import TimeDeltaWrapper

__author__ = 'anzietek'

'''

{
 __class__:Group,

    id:id  #if id==0: line==ALL name: group_(id)

    line_ids:[line1,line2] #int
    delay: time

    type:inclusive | exclusive #wlaczone w jednym czasie, naprzemian

    mode:manual|auto

    mode:{id:auto,
    trigger: cron|interval
      action: on_off| off_on
    }
    |
    mode:{
        id: manual
        action:on|off|on_off|off_on
    }
}
    -------------------------------------
    Format Json obiektu group
{
    "__class__": "Group",
    "__module__": "com.device.line",
    "delay": {
        "__class__": "TimeDelay",
        "__module__": "com.device.timedelay",
        "hours": 0,
        "minutes": 0,
        "seconds": 1,
        "weeks": 0
    },
    "id": 0,
    "line_ids": [
        1,
        2,
        3,
        4
    ],
    "mode": {
        "__bases__": [
            {
                "__class__": "Mode",
                "__module__": "com.device.line",
                "action": "on_off",
                "id": "auto"
            }
        ],
        "__class__": "AutoMode",
        "__module__": "com.device.line",
        "trigger": {
            "__bases__": [
                {
                    "__class__": "Trigger",
                    "__module__": "com.device.line",
                    "end_date": "None",
                    "start_date": "2015-10-11 12:10:10",
                    "timezone": "utc"
                }
            ],
            "__class__": "Interval",
            "__module__": "com.device.line",
            "days": 0,
            "minutes": 0,
            "seconds": 58,
            "weeks": 1
        }
    },
    "name": "group_0",
    "type": "inclusive"
}

'''

class Group(js.JsonMixin):
    # action_types = ('ex_on_off', 'ex_off_on', 'in_on_ff', 'in_off_on')
    _json_fields = [js.IntAttr('id'), js.StrAttr('name'), js.IntListAttr('line_ids'),
                    js.Attr('delay', _type=TimeDelay), js.StrAttr('type'),
                    js.Attr('mode', _type=Mode), js.BoolAttr('weather_on'),
                    js.Attr('weather_td', _type=TimeDeltaWrapper),
                    js.BoolAttr('report_on')]

    __type = ('inclusive',
              'exclusive')


    def __init__(self, id, mode, type, delay, line_ids=[], weather_on=False, weather_td=TimeDeltaWrapper(seconds=5),
                 report_on=True):
        self.__class__.__execution(id, mode, delay, line_ids, weather_on, weather_td, report_on)

    @classmethod
    def __execution(cls, id, mode, type, delay, line_ids=[], weather_on=False, weather_td=TimeDeltaWrapper(seconds=5),
                    report_on=True):
        group = cls.__new__(cls)
        group.id = id
        group.name = 'group_%s' % id
        group.mode = mode
        if type not in group.__type:
            raise ValueError('Type {} not in {}'.format(type, group.type))
        group.type = type
        group.delay = delay
        group.line_ids = line_ids
        group.weather_on = weather_on
        group.weather_td = weather_td
        group.report_on = report_on
        return group


    @classmethod
    def manual_execution(cls, id, action, type, delay, line_ids=[], report_on=True):
        '''

        :param id:
        :param action:
        :param type:
        :param delay:
        :param line_ids:
        :return:
        '''
        return cls.__execution(id=id, mode=ManualMode(action), type=type, delay=delay,
                               line_ids=line_ids, weather_on=False, weather_td=TimeDeltaWrapper(), report_on=report_on)


    @classmethod
    def auto_execution(cls, id, trigger, action, type, delay, line_ids=[], weather_on=False,
                       weather_td=TimeDeltaWrapper(), report_on=True):
        return cls.__execution(id=id, mode=AutoMode(action, trigger), type=type, delay=delay,
                               line_ids=line_ids, weather_on=weather_on, weather_td=weather_td, report_on=report_on)