import uuid
from time import sleep

from org.device.json import json_converter as js

from json import dumps


__author__ = 'anzietek'

from collections import namedtuple


class TimeDelay(js.JsonMixin):
    """
    This class give delay execution with interrupt
    """
    _TimeResolution = namedtuple('_TimeResolution', ['resolution'])
    _json_fields = [js.IntAttr('weeks'), js.IntAttr('hours'), js.IntAttr('minutes'), js.IntAttr('seconds'),
                    js.StrAttr('delay_id')]

    def __init__(self, weeks=0, hours=0, minutes=0, seconds=0):
        self.weeks = weeks
        self.hours = hours
        self.minutes = minutes
        self.seconds = seconds
        self.delay_id =str(uuid.uuid1())
        self._init()
        # self._total_seconds = weeks * 86400 + hours * 3600 + minutes * 60 + seconds
        # self._TIME_RESOLUTION = self._TimeResolution(0.5)
        # self._iter_times = int(self._total_seconds / self._TIME_RESOLUTION.resolution)
        # self._interrupt = False
        # self._delay_running=False

    def _init(self):
        self._total_seconds = self.weeks * 86400 + self.hours * 3600 + self.minutes * 60 + self.seconds
        self._TIME_RESOLUTION = self._TimeResolution(0.5)
        self._iter_times = int(self._total_seconds / self._TIME_RESOLUTION.resolution)
        self._interrupt = False
        self._delay_running = False

    def delay(self):

        """"
            delay with interrupt
        """
        self._delay_running = True
        for _ in range(self._iter_times):
            sleep(self._TIME_RESOLUTION.resolution)
            if self._interrupt:
                break
        self._interrupt = False
        self._delay_running = False


    def is_delay_running(self):
        return self._delay_running

    def interrupt(self):
        self._interrupt = True


    def get_total_delay(self):
        return self._total_seconds


    def __repr__(self):
        d = {'__class__': self.__class__.__name__,
             '__module__': self.__module__,
             'weeks': self._weeks,
             'hours': self._hours,
             'minutes': self._minutes,
             'seconds': self._seconds
        }

        return dumps(d)

