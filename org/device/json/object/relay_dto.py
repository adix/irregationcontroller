from org.device.json import json_converter as js


class RelayDTO(js.JsonMixin):
    _json_fields = [js.IntAttr('id'), js.IntAttr('on_off')]

    def __init__(self, id=0, on_off=0):
        self.id = id
        self.on_off = on_off
