import pdb

from org.device.json import json_converter as js
from org.device.config import SENSOR_PORT

try:
    from OPi import GPIO as gpio
except ImportError:
    pass

class MoistureSensor(js.JsonMixin):
    #FILE_NAME = '/sys/class/gpio_sw/{}/data'
    _json_fields = [js.BoolAttr('enabled'), js.IntAttr('mode')]
    _NO = 0
    _NC = 1
    _LOW = 0
    _HIGH = 1

    def __init__(self, enabled=False, mode=_NO):
        self.enabled = enabled
        self.mode = mode
        #self.senor_port = self.FILE_NAME.format(SENSOR_PORT)

    """
        Funkcja sprawdza czy czujnik włączony jesli tak to w zaleznosci
        od trybu normalnie otwarty/normalnie zamknęty okresla wilgotnosc gleby
    """

    def moisture(self):
        if not self.enabled:
            return False
        state = self._read_state()
        #pdb.set_trace()
        if self._NO == self.mode:
            if state == self._LOW:
                return True
        if self._NC == self.mode:
            if state == self._HIGH:
                return True
        return False

    @property
    def mode_name(self):
        return 'NO' if self.mode == self._NO else 'NC'

    def _read_state(self):
        # with open(self.FILE_NAME, 'rb') as f:
        #     return f.read()
        return gpio.input(SENSOR_PORT)
