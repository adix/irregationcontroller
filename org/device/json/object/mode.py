import uuid

from org.device.json import json_converter as js
from org.device.json.object.trigger import Trigger

__author__ = 'anzietek'


class Mode(js.JsonMixin):
    '''
    Klasa bazowa dla akcji
    '''
    _json_fields = [js.StrAttr('mode_id'), js.StrAttr('mode'), js.StrAttr('action')]

    def __init__(self, mode, action):
        self.mode_id = str(uuid.uuid1())
        self.mode = mode
        self.action = action


class ManualMode(Mode):
    '''
     Akcje wykonywane manualnie
    '''
    _json_fields = []
    __actions = ('on', 'off')#, 'on_off', 'off_on')

    def __init__(self, action):
        if action not in self.__actions:
            raise ValueError('Action {} not in {}'.format(action, self.__actions))
        super().__init__('manual', action)


class AutoMode(Mode):
    '''
     Akcje wykonywane automatycznie według trigger'a
    '''
    _json_fields = [js.Attr('trigger', _type=Trigger)]
    __actions = ('on_off', 'off_on')

    def __init__(self, action, trigger):
        if action not in self.__actions:
            raise ValueError('Action {} not in {}'.format(action, self.__actions))
        super().__init__('auto', action)
        self.trigger = trigger