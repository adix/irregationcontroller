from functools import partial
from itertools import chain
import json

__author__ = 'anzietek'


def Attr(name, _type, elem_type=None):
    '''

    :param name: nazwa atrybutu
    :param _type: typ atrybutu
    :param elem_type: w przypadku listy okresla typ elementu listy
    :return: Wrapper() opakowujacy właściwość nazwe i typ
    '''
    _name = '_' + name

    if elem_type and _type != list:
        raise ValueError('Arg elem_type only use for list type')

    @property
    def prop(self):
        return getattr(self, _name)

    @prop.setter
    def prop(self, value):
        if value != None and not isinstance(value, _type):
            raise TypeError('Expected %s but recived %s' % (
                _type, type(value)))
        if isinstance(value, list) and elem_type:
            tmp = []
            for elem in value:
                tmp.append(elem_type(elem))
            value[:] = tmp

        setattr(self, _name, value)

    class Wrapper:
        def __init__(self):
            self.prop = prop
            self.name = name
            self.type = _type

    return Wrapper()


BoolAttr = partial(Attr, _type=bool)
IntAttr = partial(Attr, _type=int)
FloatAttr = partial(Attr, _type=float)
StrAttr = partial(Attr, _type=str)
ListAttr = partial(Attr, _type=list)
IntListAttr = partial(Attr, _type=list, elem_type=int)

Dict = partial(Attr, _type=dict)


# def register_type(cls):
# setattr(sys.modules[__name__], cls.__name__, partial(property_type, _type=cls.__class__))
# return cls

def register_field(name, clazz):
    def decorator(cls):
        setattr(cls, name, clazz)
        return cls

    return decorator


class JsonMeta(type):
    '''
    Metaklasa tworzaca właściwosci na podstawie cls._json_fields
    dodatkowo rejestruje klase w globalnym słowniku
    registered_clazz umozliwiajac znajdowanie klas bazowych
    '''
    registered_clazz = {}

    def __new__(cls, clsname, bases, clsdict):
        cls = type.__new__(cls, clsname, bases, clsdict)
        JsonMeta.registered_clazz.update({cls.__name__: cls})
        # if not hasattr(cls, '_get_all_json_fields'):
        # return cls
        for field in cls._json_fields:  # _get_all_json_fields():
            setattr(cls, field.name, field.prop)

            setattr(cls, 'type', cls)
        return cls


class JsonMixin(metaclass=JsonMeta):
    '''
        Klasa bazowa serializujaca i deserializujaca do formatu json
    '''
    _simple_types = (int, float, str, bool)  # , list)
    _json_fields = []

    def _init(self):
        pass

    @classmethod
    def _get_all_json_fields(cls):
        fields = []
        if hasattr(cls, '_json_fields'):
            json_fields = cls._json_fields
            if json_fields:
                fields.append(json_fields)
        if hasattr(cls, '__bases__'):
            for base in cls.__bases__:
                if hasattr(base, '_json_fields'):
                    json_fields = base._json_fields
                    fields.append(json_fields)
        return chain.from_iterable(fields)

    @classmethod
    def json2object(cls, _json):
        _json = json.loads(_json) if isinstance(_json, str) else _json
        clsname = _json.pop('__class__', None)
        bases = _json.pop('__bases__', [])
        if not clsname:
            raise TypeError('Conversion json to %s is not possible' % cls)
        if cls.__name__ != clsname:
            newcls = cls.registered_clazz.get(clsname, None)  # cls is A newcls is B  class B(A)
            if newcls in cls.__bases__:
                cls = newcls
            elif cls in newcls.__bases__:
                cls = newcls
            else:
                raise TypeError('Conversion is not possible %s is not subtype of %s' % (newcls, cls))
        clsbases = [object]
        clsdict = {}
        obj = None
        for base in bases:
            obj = cls.json2object(base)
            if obj:
                clsbases.append(obj.__class__)
                clsdict.update(obj.__dict__)

        obj = cls.__new__(cls, cls.__name__, bases=tuple(clsbases))
        # obj.__init__()
        # obj.__bases__=tuple(clsbases)
        obj.__dict__.update(clsdict)

        for field in cls._json_fields:
            json_elem = _json.get(field.name, None)
            if field.type in cls._simple_types or json_elem is None:
                setattr(obj, field.name, None if isinstance(json_elem, type(None)) else field.type(json_elem))
            elif isinstance(json_elem, list):
                prop = []
                for elem in json_elem:
                    if not hasattr(elem, 'get'):
                        prop.append(elem)
                        continue
                    clsname = elem.get('__class__', None)
                    newcls = cls.registered_clazz.get(clsname, None)
                    if newcls and hasattr(newcls, 'json2object'):
                        prop.append(newcls.json2object(json.dumps(elem)))
                    else:
                        elem = elem if isinstance(elem, dict) else json.loads(elem)
                        prop.append(elem)
                setattr(obj, field.name, prop)
            else:
                json_elem = json_elem if isinstance(json_elem, dict) else json.loads(json_elem)
                prop = {}
                if hasattr(json_elem, 'get'):
                    clsname = json_elem.get('__class__', None)

                    newcls = cls.registered_clazz.get(clsname, None)
                    if newcls and hasattr(newcls, 'json2object'):
                        prop = newcls.json2object(json.dumps(json_elem))  # field.type
                    else:
                        prop = json_elem
                else:
                    prop = json_elem
                setattr(obj, field.name, prop)
        if hasattr(obj, '_init'):
            obj._init()
        return obj

    def object2json(self, as_str=True):
        cls = self.__class__
        _json = {'__class__': cls.__name__,
                 '__module__': self.__module__}

        bases = filter(lambda cls: hasattr(cls, '_json_fields') and cls != JsonMixin, cls.__bases__)

        clslbases = []
        for base in bases:
            fields = base._json_fields
            _base_json = {'__class__': base.__name__,
                          '__module__': base.__module__}
            for field in fields:
                name = field.name
                attr = getattr(self, name)
                value = attr
                if attr and hasattr(attr, 'object2json'):
                    value = attr.object2json()
                    value = json.loads(value)
                _base_json.update({field.name: value})
            clslbases.append(_base_json)
        if clslbases:
            _json.update({'__bases__': clslbases})
        for field in cls._json_fields:  # self._json_fields:
            attr = getattr(self, field.name, None)  # check type(attr) == field.type
            if attr != None and hasattr(attr, '__class__'):
                if type(attr) != field.type and field.type not in attr.__class__.__bases__:
                    raise TypeError('Expected %s type for attr %s' % (field.type, field.name))
            value = None

            if attr and hasattr(attr, 'object2json'):
                value = attr.object2json()
                value = json.loads(value)

            elif isinstance(attr, list):  # jeszcze powinno byc dla dict'a
                value = []
                for el in attr:
                    if hasattr(el, 'object2json'):

                        value.append(json.loads(el.object2json()))
                    else:
                        value.append(el)
            else:
                value = attr
            _json.update({field.name: value})
        if as_str:
            return json.dumps(_json, indent=4, sort_keys=True)
        else:
            return _json
