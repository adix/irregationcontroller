import json
from multiprocessing import Queue
from multiprocessing.connection import Listener
from threading import Thread

import requests
from org.device.channel.thingspeak_channel import ThingSpeakChannelMixin

thingSpeakQueue = Queue()


class ThingSpeakService(ThingSpeakChannelMixin):
    url = 'https://api.thingspeak.com/update.json'
    api_key = 'CSR9CY3NTTEDNLPD'
    listener = Listener(('localhost',25000),authkey=b'auth')

    def __init__(self):
        self.service = Thread(target=self.__execute)
        #self.service.daemon = True
        self.__fieldMapper = {}
        for i in range(8):
            self.__fieldMapper[str(i)] = 'field' + str(i+1)

    def __execute(self):
        while True:
            client=self.listener.accept()
            item = client.recv()
            if item:
                try:
                    item = json.loads(item)
                    data = {'key': self.api_key}
                    field_id = self.__fieldMapper[item['line_id']]
                    data[field_id] =item['state']
                    requests.post(self.url, data)
                except Exception as e:
                    pass
                    # ToDo
                    # dodac logowanie

    def run(self):
        self.service.start()


def main():
    ts= ThingSpeakService()
    ts.run()


if __name__ == '__main__':
    main()