import json
import urllib
from urllib import request, parse

from org.device.util import logger

__author__ = 'anzietek'

API_KEY = '253efb9c2fca58135d84c1ed24be4d65'


class WeatherService:
    endpoint_URL = 'http://api.openweathermap.org/data/2.5'
    LANGS = ('en', 'ru', 'it', 'es', 'sp', 'uk', 'ua',
             'de', 'pt', 'ro', 'pl', 'fi', 'nl', 'fr',
             'bg', 'sv', 'se', 'zh_tw', 'zh', 'zh_cn', 'tr', 'hr', 'ca')
    MODE = ('xml', 'json', 'html')
    UNITS = ('metric', 'imperial')

    def __init__(self, path, api_key='253efb9c2fca58135d84c1ed24be4d65', lang='pl', mode='json', units='metric'):
        self._api_key = api_key
        self._lang = lang
        self._mode = mode
        self._units = units
        self._query_args = {'appid': api_key,
                            'lang': lang,
                            'mode': mode,
                            'units': units}
        self.__base_url = self.endpoint_URL + path
        self._url = lambda args: self.__base_url + '?' + args

    def endpoint_action(self, args):
        result = {}
        url_args = urllib.parse.urlencode(args)
        url = self._url(url_args)
        logger.info('Url -> {}'.format(url))
        response = request.urlopen(url).read()
        if args['mode'] == 'json':
            result = json.loads(str(response, encoding='utf-8'))
        else:
            result = response
        logger.info('Pobrane dane pogodowe -> {}'.format(result))
        return result

    @property
    def query_args(self):
        return self._query_args.copy()

    def weater_by_id(self, id):
        pass

    def weather_by_city(self, city, country_code):
        pass

    def weather_by_coord(self, lat, lon):
        pass

    def weather_by_zip(self, zip, country_code):
        pass


class CurrentWeather(WeatherService):
    def __init__(self, *args, **kwargs):
        super().__init__(path='/weather', *args, **kwargs)

    def weather_by_zip(self, zip, country_code):
        '''

        :param zip:
        :param country_code:
        :return: str|dict   str when mode=='xml' or dict when mode == 'xml'
        '''
        args = self.query_args
        args.update({'zip': zip + ',' + country_code})
        return super().endpoint_action(args)

    def weather_by_city(self, city, country_code):
        '''

        :param city:
        :param country_code:
        :return:
        '''
        args = self.query_args
        args.update({'q': city + ',' + country_code})
        return super().endpoint_action(args)

    def weater_by_id(self, id):
        args = self.query_args
        args.update({'id': id})
        return super().endpoint_action(args)

    def weather_by_coord(self, lat, lon):
        args = self.query_args
        args.update({'lat': lat})
        args.update({'lon': lon})
        return super().endpoint_action(args)
