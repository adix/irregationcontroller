import json
from pymongo import MongoClient, DESCENDING
from org.device.config import DB_HOST, DB_PORT, DB_NAME
from org.device.util.logging import LoggerMixin


class MongoDb(LoggerMixin):
    def __init__(self):
        self.client = MongoClient(DB_HOST, DB_PORT)
        self.db = self.client[DB_NAME]
        pass

    def save_groups(self, groups):
        with self.client:
            try:
                self.db['groups'].drop()
                self.db['groups'].insert({'groups': groups})
                # self.db['groups'].replace_one({'_id': 0}, {'groups': groups}, upsert=True)
            except Exception as e:
                self.logger.info(e)
                raise e

    def save_sensorsetting(self, sensor):
        with self.client:
            try:
                self.db['sensor'].drop()
                self.db['sensor'].insert({'sensor': sensor})
                # self.db['groups'].replace_one({'_id': 0}, {'groups': groups}, upsert=True)
            except Exception as e:
                self.logger.info(e)
                raise e

    def save_pumpsetting(self, pump):
        with self.client:
            try:
                self.db['pump'].drop()
                self.db['pump'].insert({'pump': pump})
                # self.db['groups'].replace_one({'_id': 0}, {'groups': groups}, upsert=True)
            except Exception as e:
                self.logger.info(e)
                raise e

    def save_reportsetting(self, report):
        with self.client:
            try:
                self.db[report[0]].drop()
                self.db[report[0]].insert({report[0]: report[1]})
                # self.db['groups'].replace_one({'_id': 0}, {'groups': groups}, upsert=True)
            except Exception as e:
                self.logger.info(e)
                raise e

    def save_weathersetting(self, weather):
        with self.client:
            try:
                self.db['weather'].drop()
                self.db['weather'].insert({'weather': weather})
                # self.db['groups'].replace_one({'_id': 0}, {'groups': groups}, upsert=True)
            except Exception as e:
                self.logger.info(e)
                raise e

    def load_sensorsetting(self):
        with self.client:
            # {'_id': 0}
            return self.db['sensor'].find_one()

    def load_pumpsetting(self):
        with self.client:
            # {'_id': 0}
            return self.db['pump'].find_one()

    def load_reportsetting(self, name):
        with self.client:
            # {'_id': 0}
            return self.db[name].find_one()

    def load_weathersetting(self):
        with self.client:
            # {'_id': 0}
            return self.db['weather'].find_one()

    def load_groups(self):
        with self.client:
            # {'_id': 0}
            return self.db['groups'].find_one()

    def load_reports(self):
        with self.client:
            cursor = self.db['reports'].find({}, {'_id': 0, 'date': 0}).sort([('date', DESCENDING)]).limit(20)
            return {'reports': list(cursor)}
