class Pomp:
    def __init__(self,_enabled=False):
        self._enabled=_enabled

    @property
    def ENABLED(self):
        return self._enabled

    @ENABLED.setter
    def ENABLED(self, value):
        self._enabled = value