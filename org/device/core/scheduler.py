import json
import pdb
import threading
from apscheduler.schedulers.background import BackgroundScheduler
from org.device.channel.channel import Channel, QType
from org.device.core.context import Context
from org.device.core.line import InclusiveLinesAction, ExclusiveLinesAction, LinesManager
from org.device.json import json_converter as js
from org.device.json.object.group import Group
# from org.device.util.logging import logger, func_name
from org.device.json.object.mode import AutoMode, ManualMode
from org.device.channel.nullqueue import NullQueue
from org.device.job.report_job import ReportJob
from org.device.job.weather_job import WeatherJob, WeatherJobByZipOrId
from org.device.json.object.moisture_sensor import MoistureSensor

__author__ = 'anzietek'


class SchedulerIsNotStarted(Exception):
    pass


class AppScheduler(js.JsonMixin):
    _json_fields = [js.Attr('groups', _type=list), js.Attr('weather_job', _type=WeatherJob),
                    js.Attr('report_job', _type=ReportJob)]
    _type = {
        'inclusive': lambda group_name, lines, delay, sensor: InclusiveLinesAction(group_name, lines, delay, sensor),
        'exclusive': lambda group_name, lines, delay, sensor: ExclusiveLinesAction(group_name, lines, delay, sensor)}

    _action = {'on_off': lambda obj: obj.action_on_off,
               'off_on': lambda obj: obj.action_off_on,
               'on': lambda obj: obj.action_on,
               'off': lambda obj: obj.action_off}

    _type_cache = {}
    _job_cache = {}
    _group_cache = {}

    def __init__(self, city='', scheduler=BackgroundScheduler(), groups=[], channel=None, report_on=True):
        # self._ctx = Context(len(groups))
        # self._ctx.validate(groups)
        # self._lines_manager = LinesManager(self._ctx.groups_number)
        self.lock = threading.Lock()
        self.groups = groups
        self.sensor = MoistureSensor()
        self.weather_job = WeatherJobByZipOrId()

        self._scheduler = scheduler
        # self.report_on = report_on
        self._init(channel)

    def _init(self, channel=None):
        self._ctx = Context(len(self.groups))
        self._ctx.validate(self.groups)
        self._lines_manager = LinesManager(self._ctx.groups_number)
        # self._scheduler = scheduler
        self._channel = channel if channel else Channel()

        # self.weather_job.channel = self._channel
        # self.weather_job.on = True
        self.report_job = ReportJob()  # DummyReportJob()

        self.__update_weather_job()
        self.__update_report_job()

        for group in self.groups:
            for line_id in group.line_ids:
                self._lines_manager.move_line_to_group(line_id=line_id, group_name=group.name)
            self.__update_group_job(group)
            self.__update_weather_channel(group)
            # self.__update_report_channel(group)

    def __update_weather_job(self):
        # if self.weather_on:
        self.weather_job.channel = self._channel
        if self.__get_job(job_id='weather_job'):
            # self._scheduler.remove_job(job_id='weather_job')
            #
            self._scheduler.modify_job(job_id='weather_job', name='weather_job', func=self.weather_job.weather_action,
                                       trigger=self.weather_job.trigger.name)
            self._scheduler.reschedule_job(job_id='weather_job', trigger=self.weather_job.trigger.name)
        else:
            self._scheduler.add_job(id='weather_job', name='weather_job', func=self.weather_job.weather_action,
                                    trigger=self.weather_job.trigger.name)

    def weather_task(self, weather_job):
        self.weather_job = weather_job
        self.__update_weather_job()

    def __update_report_job(self):
        self.report_job.channel_in = self._channel.register_queue('REPORT', QType.REPORT)
        if self.__get_job(job_id='report_job'):
            self._scheduler.modify_job(job_id='report_job', name='report_job', func=self.report_job.report_action,
                                       trigger=self.report_job.trigger.name)
            self._scheduler.reschedule_job(job_id='report_job', trigger=self.report_job.trigger.name)
        else:
            self._scheduler.add_job(id='report_job', name='report_job', func=self.report_job.report_action,
                                    trigger=self.report_job.trigger.name)

    def report_job(self, report_job):
        self.report_job = report_job
        self.__update_report_job()

    def report_job_trigger(self, trigger):
        self.report_job.trigger = trigger
        self.__update_report_job()

    def get_report_job_trigger(self):
        return self.report_job.trigger

    def report_generator(self, rgen):
        return self.report_job.report_generator(rgen)

    def get_report_generator(self, name):
        return self.report_job.get_report_generator(name)

    # @property
    # def report_job(self):
    #     return  self.report_job

    def start(self):
        self.__start()

    def get_linesaction_object(self, group):
        '''
        zapisuje w cache obiekt typu LinesAction pod kluczem group.name
        co pozwoli na pozniejsze przerwanie w lines action
        :param group:
        :return:
        '''
        # queue = self.weather_job.channel.register_queue(group.name) if self.weather_on else None
        _type = self._type[group.type](group.name, self._lines_manager.lines(group.name), group.delay,
                                       self.sensor)
        self._type_cache[group.name] = _type
        return _type

    def __update_weather_channel(self, group):
        weather_on = False
        if isinstance(group.mode, AutoMode):
            weather_on = group.weather_on
        _type = self._type_cache.get(group.name, None)
        if _type:
            queue = self._channel.register_queue(group.name, QType.WEATHER)
            if weather_on:
                _type.channel_in = queue
                _type.timedelta = group.weather_td
            else:
                _type.channel_in = NullQueue()

    def weather_channel(self, group):
        self.__update_weather_channel(group)

    def __update_report_channel(self, group):
        _type = self._type_cache.get(group.name, None)
        if _type:
            queue = self._channel.register_queue('REPORT', QType.REPORT)
            if group.report_on:
                _type.channel_out = queue


            else:
                _type.channel_out = NullQueue()

    def report_channel(self, group):
        self.__update_report_channel(group)

    def __get_job_action(self, group):
        '''
        Funkcja na podstawie obiektu group tworzy job dla schedulera

        :param group: grupa dla ktorej tworzony jest job
        :return: job do wykonania
        '''
        _type = self.get_linesaction_object(group)
        job = self._action[group.mode.action](_type)
        self._job_cache[group.name] = job
        return job

    def __interrupt(self, group_name):
        '''
        Funkcja przerywa dzialanie lines action
        dla podanej nazwy grupy
        :param group_name: nazwa grupy
        :return:
        '''
        lines_action = self._type_cache.get(group_name, None)
        if lines_action:
            lines_action.action_interrupt()

    def __get_job(self, job_id):
        return self._scheduler.get_job(job_id=job_id)

    def get_jobs(self):
        return self._scheduler.get_jobs()

    def __pause_job(self, job_id):
        job = self.__get_job(job_id)
        if job and job.next_run_time != None:
            self._scheduler.pause_job(job_id=job_id)
            return job_id
        return None

    def __remove_job(self, job_id):
        if self.__get_job(job_id):
            self._scheduler.remove_job(job_id)

    def __resume_job(self, job_id):
        if self.__get_job(job_id):
            self._scheduler.resume_job(job_id=job_id)

    def modify_group(self, group):
        '''
        :param group: Group|list(Group)
        :return:
        '''
        if not self.running:
            raise SchedulerIsNotStarted()
        if isinstance(group, str):
            groups = json.loads(group);
            for gr in groups:
                self.__update_group(Group.json2object(gr))
        else:
            self.__update_group(group)

    def moisture_sensor(self,sensor):
        #self.sensor = sensor
        self.sensor.enabled = sensor.enabled
        self.sensor.mode=sensor.mode

    def get_moisture_sensor(self):
        return self.sensor

    def get_lines_state(self):
        return self._lines_manager.lines_state()

    def __update_group(self, group):
        '''
        Funkcja przenosi linie dst_group.line_ids
        z innych grup do dst_group

        :param group: grupa docelowa(modyfikowana)
        :return:
        '''
        self._ctx.validate_group(group)
        line_ids = group.line_ids
        dst_group_name = group.name

        group_line = []  # zawiera tuple(src_group_name, line_id)

        # wyszukanie wszystkich grup związanych z line_ids
        for line_id in line_ids:
            src_group_name = self._lines_manager.find_group(line_id)
            if src_group_name and src_group_name != dst_group_name:
                group_line.append((src_group_name, line_id))

        # self._type_cache[dst_group_name].action_interrupt()

        self.__interrupt(dst_group_name)  # przerwanie działania grupy doce}lowej w celu modyfikcaji

        # job = self.__get_job_action(group)

        if not self.running:
            self.__start()

            # jezeli scheduler jest uruchomiony wstrzymuje zadania nastepnie je modyfikuje i ponownie wznawia

            # wlaczyc czekanie
            # _type = self._type[group.type](group.name,self._lines_manager.lines(group.name), group.delay)
            # self._type_cache[group.name] = _type
            # job_action = self._action[group.action](_type)
            # self._job_cache[group.name] = job_action
            # job = self.get_job(dst_group)
        jobs_to_resume = []
        if isinstance(group.mode, AutoMode):
            job_id = self.__pause_job(dst_group_name)
            if job_id:
                jobs_to_resume.append(job_id)
        elif isinstance(group.mode, ManualMode):
            self.__remove_job(dst_group_name)

        for src_group_name, line_id in group_line:
            # self._type_cache[src_group_name].action_interrupt()
            self.__interrupt(src_group_name)
            job_id = self.__pause_job(job_id=src_group_name)
            if job_id:
                jobs_to_resume.append(job_id)
            self._lines_manager.move_line_to_group(line_id, dst_group_name)

            # self._scheduler.modify_job(job_id=dst_group_name, func=self._job_cache[dst_group_name])

        self.__update_group_job(group)
        self.__update_weather_channel(group)
        # self.__update_report_channel(group)

        # self.__resume_job(dst_group_name)
        #
        for job_id in jobs_to_resume:
            self.__resume_job(job_id=job_id)

    @property
    def running(self):
        return self._scheduler.running

    def __update_group_job(self, group):
        job_action = self.__get_job_action(group)
        self.__update_report_channel(group)
        if isinstance(group.mode, AutoMode):
            if self.__get_job(group.name):
                self._scheduler.modify_job(job_id=group.name, name=group.name, func=job_action,
                                           trigger=group.mode.trigger.name)
                self._scheduler.reschedule_job(job_id=group.name,
                                               trigger=group.mode.trigger.name)

            else:
                self._scheduler.add_job(id=group.name, name=group.name, func=job_action,
                                        trigger=group.mode.trigger.name)
        elif isinstance(group.mode, ManualMode):
            job_action()
        # group.line_ids=self._lines_manager.line_ids(group.name)

        self._group_cache[group.name] = group
        for name, group in self._group_cache.items():
            group.line_ids = self._lines_manager.line_ids(group.name)
            self._group_cache[name] = group

        self.groups = list(self._group_cache.values())

    def __start(self):
        self._scheduler.start()

    def pause_all(self):
        if not self.running:
            raise SchedulerIsNotStarted()
        jobs = self.get_jobs()
        for job in jobs:
            self.__pause_job(job_id=job.id)

    def resume_all(self):
        if not self.running:
            raise SchedulerIsNotStarted()
        jobs = self.get_jobs()
        for job in jobs:
            self._scheduler.resume_job(job_id=job.id)

    def shutdown(self, wait=True):
        # logger.debug('%s.%s' % (type(self).__name__, func_name()))
        if not self.running:
            raise SchedulerIsNotStarted()
        for lines_action in self._type_cache.values():
            lines_action.action_interrupt()
        self._scheduler.shutdown(wait)
