# from pyA20.gpio import gpio, port
from org.device.config import RELAY_PORT_MAPPER, DEV_MODE, POMP_PORT, ORANGEPI_ZERO, POMP

__author__ = 'anzietek'

try:
    from OPi import GPIO as gpio
except ImportError:
    pass


# gpio.init()


class _RelayState:
    _OFF = b'0'
    _ON = b'1'


class _Relay:
    FILE_NAME = '/sys/class/gpio_sw/{}/data'

    # PORT_MAPPER = {'A': 'PA8', 'B': 'PA9', 'C': 'PA10', 'D': 'PA20'}

    def __init__(self, port_id, bit):
        self.relay_port = self.FILE_NAME.format(RELAY_PORT_MAPPER[port_id])
        self.pomp_port = self.FILE_NAME.format(POMP_PORT)
        self.set(bit)

    def set(self, bit):
        with open(self.relay_port, 'wb') as f:
            f.write(bit)
        if POMP.ENABLED:
            with open(self.pomp_port, 'wb') as f:
                f.write(bit)


class _RelayPiZero:
    def __init__(self, port_id, bit):
        if port_id == 'E':
            self._relay = POMP_PORT
        else:
            self._relay = RELAY_PORT_MAPPER[port_id]

        self._pomp = POMP_PORT
        self._mapper = {_RelayState._ON: gpio.HIGH, _RelayState._OFF: gpio.LOW}

    def set(self, bit):
        gpio.output(self._relay, self._mapper[bit])
        if POMP.ENABLED:
            gpio.output(self._pomp, self._mapper[bit])


# class _Relay:
#     PORTS = {'A': port.PA8, 'B': port.PA9, 'C': port.PA10, 'D': port.PA20}
#
#     def __init__(self, port, state):
#         self.port = self.PORTS[port]
#         gpio.setcfg(self.port, gpio.OUTPUT)
#         gpio.output(self.port, state)
#
#     def set(self, state):
#         gpio.output(self.port, state)


class _DummyRelay:
    def set(self, bit):
        pass


class Relay:
    _ON = _RelayState._ON
    _OFF = _RelayState._OFF

    def __init__(self, port_id):
        self.__port_id = port_id
        self._state = self._OFF
        if DEV_MODE:
            self.__relay = _DummyRelay()
        else:
            if ORANGEPI_ZERO:
                self.__relay = _RelayPiZero(port_id, self._state)
            else:
                self.__relay = _Relay(port_id, self._state)

    def relay_on(self):
        self._state = self._ON
        self.__relay.set(self._state)

    def relay_off(self):
        self._state = self._OFF
        self.__relay.set(self._state)

    def toggle(self):
        # mask = 0x1  # mask last bit
        # self._state = ~self._state.value & mask
        self._state = self._OFF if self._ON else self._ON
        self.__relay.set(self._state)

    @property
    def state(self):
        return self._state

    def __str__(self):
        position = 'ON' if self._state == self._ON else 'OFF'
        msg = 'Relay on port={} is {}\n'.format(self.__port_id, position)
        if POMP.ENABLED:
            msg += 'Pomp is {}'.format(position)
        return msg

    def __repr__(self):
        return '<{}({!r})>'.format(self.__class__.__name__, self.__port_id)
