from org.device.json.object.group import Group

__author__ = 'anzietek'


class Context:
    def __init__(self, lines_number):
        self.__groups_number = self.__lines_number = lines_number

    def validate_line_ids(self, line_ids=[]):
        if not isinstance(line_ids, list):
            raise TypeError('%s is not list type' % type(line_ids))
        not_exist = []
        for line_id in line_ids:
            if line_id not in range(self.__lines_number):
                not_exist.append('Line(id=%s)' % line_id)
        if not_exist:
            msg = ' [ ' + ' , '.join(not_exist) + ' ] '
            raise ValueError(msg + 'not exists')

    def validate_group_id(self, group_id):
        if group_id not in range(self.__groups_number):
            raise ValueError('group_%s not exist' % group_id)

    def validate_group(self, group):
        if not isinstance(group, Group):
            raise TypeError('%s is not Group type' % type(group))
        self.validate_group_id(group.id)
        self.validate_line_ids(group.line_ids)

    def validate(self, groups):
        if isinstance(groups, list):
            if not len(groups):
                raise ValueError('groups is empty')
            for group in groups:
                self.validate_group(group)
        elif isinstance(groups, dict):
            if len(groups.keys()):
                raise ValueError('groups is empty')
            for group in groups.values():
                self.validate_group(group)
        else:
            raise TypeError('%s is not a list or a dict type' % type(groups))

    @property
    def groups_number(self):
        return self.__groups_number
