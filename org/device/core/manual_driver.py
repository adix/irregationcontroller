from org.device.core.relay import Relay


class ManualDriver:
    def execute(self, relayDTO):
        id = relayDTO.id
        on_off = relayDTO.on_off
        relay = self._relay(id)
        if on_off:
            relay.relay_on()
        else:
            relay.relay_off()
        return True

    def _relay(self, id):
        port = chr(ord('A') + id)
        return Relay(port)
