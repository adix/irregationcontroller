from abc import ABCMeta, abstractmethod
import copy
from datetime import datetime, timedelta
import threading
from org.device.channel.channel import QueueChannelMixin
from org.device.channel.thingspeak_channel import ThingSpeakChannelMixin
from org.device.json.object.line_item import LineLogItem
from org.device.json.object.moisture_sensor import MoistureSensor
from org.device.json.object.thingspeak_item import ThingSpeakItem
from org.device.util.logging import LoggerMixin, func_name
from org.device.channel.nullqueue import NullQueue
from org.device.core.relay import Relay
from org.device.json.object.timedelay import TimeDelay

__author__ = 'anzietek'


class Line(LoggerMixin):
    '''
    Klasa reprezentuje logiczne  linie do ktorej są podpięte przekazniki
    '''

    def __init__(self, id=1, relays=[]):
        '''

        :param id: identyfikator lini
        :param relays: lista przekaznikow podpieta pod ta linie
        '''
        self.__id = id
        self.__relays = relays

    def line_on(self):
        '''
         załączenie lini
        '''
        # self.logger.info('id=%s ON' % self.id)
        for relay in self.__relays:
            relay.relay_on()

    def line_off(self):
        '''

        wyłączenie lini
        '''
        # self.logger.info('id=%s OFF' % self.id)
        for relay in self.__relays:
            relay.relay_off()

    def line_toggle(self):

        for relay in self.__relays:
            relay.toggle()

    def line_state(self):
        state = None
        for relay in self.__relays:
            state |= relay.state
        return state

    @property
    def id(self):
        return self.__id

    def __str__(self):
        return '%s(id=%s)' % (type(self).__name__, self.__id)


class LineFactory:
    '''
     Fabryka lini
    '''

    @classmethod
    def create_line(cls, id):
        port = chr(ord('A') + id)
        return Line(id, (Relay(port),))


class LinesManager:
    '''
     Klasa umozliwiajaca zarzadzanie liniami poprzez grupy.
     Linie sa przypisane do odpowiedniej grupy

    '''

    def __init__(self, groups_number):
        '''
        Liczba grup rowna liczbie linii, każda grupa posiada jedna linie
        np. dla 3 grup
         gruop_0=[Line(id=0)]
         group_1=[Line(id=1)]
         group_2=
        :param groups_number: liczba grup
        :return:
        '''
        self.__groups_number = groups_number  # lines_number
        self.__groups = self.__init_groups()

    def __create_default_groups(self):
        '''
        tworzy domyslne odwzorowanie grupa linia
        :return:dict  domyslna liczba grup
        '''
        return {'group_' + str(group_id): [LineFactory.create_line(group_id)] for group_id in
                range(self.__groups_number)}

    def __init_groups(self):
        if not hasattr(self, '__default_groups'):
            self.__default_groups = self.__create_default_groups()

        return copy.deepcopy(self.__default_groups)

    def restore_to_default_groups(self):
        '''
         Restore to default
        '''
        self.__groups = self.__init_groups()

    def move_line_to_group(self, line_id, group_name):
        '''
         przenosi linie  do grupy
         :param line_id: linia przenoszona
         :param group_name: grupa do ktorej linia zostanie przeniesiona
        '''

        if self.groups.get(group_name, None) == None:
            return
        line = self.__get_line_and_remove(line_id)
        if line:
            self.groups[group_name].append(line)

    # @property
    def lines(self, group_id):
        '''
        :return: zwraca liste lini przypisanych do grupy
        '''
        return self.__groups.get(group_id)

    def line_ids(self, group_id):
        return [line.id for line in self.__groups.get(group_id)]

    def lines_state(self):
        result = []
        for _, lines in self.__groups.items():
            for line in lines:
                result.append((line.id, line.line_state()))
        return result

    @property
    def group_ids(self):
        '''
        :return: zwraca liste grup
        '''
        return range(self.__groups_number)

    @property
    def group_names(self):
        return self.__groups.keys()

    @property
    def groups(self):
        return self.__groups

    def __get_line_and_remove(self, line_id):
        '''
        :param line_id: id lini ktora nalezy usunac z grupy
        :return: zwraca usunieta linie lub None
        '''
        line = None
        for lines in self.__groups.values():
            for i, line in enumerate(lines):
                if line.id == line_id:
                    line = lines[i]
                    del lines[i]
                    return line
        return None

    def find_group(self, line_id):
        '''
        wyszukanie grupy do ktorej nalezy podana linia
        :param line_id: id linii
        :return: zwraca grupe do ktorej nalezy linia lub None
        '''

        for group, lines in self.__groups.items():
            for i, line in enumerate(lines):
                if line.id == line_id:
                    return group
        return None


class LinesAction(LoggerMixin, QueueChannelMixin,metaclass=ABCMeta):
    '''
    Abstrakcyjna klasa z akcjami on_off i off_on
    LinesAction(LoggerMixin, QueueChannelMixin, ThingSpeakChannelMixin, metaclass=ABCMeta)
    '''

    def __init__(self, group_name, lines=[], time_delay=None, sensor=None):
        if not isinstance(time_delay, TimeDelay):
            raise Exception('Param execution_time should be type of TimeDelay')
        self._execution_time = time_delay
        self._lines = lines
        self._group_name = group_name
        self._rainy = False
        self.sensor = MoistureSensor() if not sensor else sensor
        self._time_delta = timedelta(hours=3)
        self._is_executed = False
        self.cv = threading.Condition()

    @abstractmethod
    def action_on_off(self):
        '''
        abstrakcyjna akcja włacz wylacz
        '''
        with self.cv:
            self._is_executed = True
        self._rainy = self.rainy

    @abstractmethod
    def action_off_on(self):
        '''
        abstrakcyjna akcja wylacz wlacz
        '''
        with self.cv:
            self._is_executed = True
        self._rainy = self.rainy

    def action_on(self):
        name = 'exclusive'
        if isinstance(self, InclusiveLinesAction):
            name = 'inclusive'
        log_item = LineLogItem(name, self._group_name, 'on', TimeDelay(), False)
        log_item.date_on = log_item.now()
        lines_on = []
        for line in self._lines:
            line.line_on()
            lines_on.append(line.id)
            #self.ts_put_nowait(ThingSpeakItem(str(line.id), '0,1'))
        log_item.lines_on = lines_on
        log_item.lines_off = []
        log_item.date_off = ''
        self.put_nowait(log_item.object2json())

    def action_off(self):
        name = 'exclusive'
        if isinstance(self, InclusiveLinesAction):
            name = 'inclusive'
        log_item = LineLogItem(name, self._group_name, 'off', TimeDelay(), False)
        log_item.date_off = log_item.now()
        lines_off = []
        for line in self._lines:
            line.line_off()
            lines_off.append(line.id)
            #self.ts_put_nowait(ThingSpeakItem(str(line.id), '1,0'))
        log_item.lines_on = []
        log_item.lines_off = lines_off
        log_item.date_on = ''
        self.put_nowait(log_item.object2json())

    # @abstractmethod
    def action_interrupt(self):
        '''
        przerwanie wykonywanej akcji
        '''

        self.logger.info(func_name())
        with self.cv:
            while self._is_executed:
                self._execution_time.interrupt()
                self.cv.wait()

    @property
    def timedelta(self):
        return self._time_delta

    @timedelta.setter
    def timedelta(self, value):
        if value and isinstance(value, timedelta):
            self._time_delta = value

    # @property
    # def weather_on(self):
    # return self._weather_on
    # @weather_on.setter
    # def weather_on(self, value):
    # if not isinstance(value, bool):
    # raise TypeError('%s must be bool type' % value)
    # self._weather_on = value
    @property
    def moisture(self):
        state = self.sensor.moisture()
        self.logger.info(func_name() + ' return ' + str(state))
        return state

    @property
    def rainy(self):
        if isinstance(self.channel_in, NullQueue):
            return False
        # try:
        dt = None
        rain = False
        while True:
            msg = self.get_nowait()
            if msg:
                # dt = datetime.fromtimestamp(msg[0])
                rain = msg[1]
                self.logger.info(
                    func_name() + ' ' + self._group_name + ' recived: msg( ' + str(msg[0]) + ',' + str(rain) + ' )')
                if isinstance(msg[0], int) and isinstance(rain, bool):
                    dt = datetime.fromtimestamp(msg[0])
                    # delta = timedelta(seconds=1)
                    diff_time = datetime.now() - dt
                    self.logger.info(
                        func_name() + ' diff_time: ' + str(diff_time) + ' diff_time < time_delta ' + str(
                            diff_time < self.timedelta))
                    if rain and diff_time < self.timedelta:
                        self.logger.info(func_name() + ' return True for ' + self._group_name)
                        return True
            else:
                break
        # self.logger.info('Queue is empty for ' + self._group_name)
        self.logger.info(func_name() + ' return False for ' + self._group_name)
        return False

    def __str__(self):
        sline = [str(line) for line in self._lines]
        prefix = '%s = [ ' % self._group_name
        return prefix + ','.join(sline) + ' ]'


class InclusiveLinesAction(LinesAction):
    '''
      W InclusiveLinesAction akcjie wykonywane sa jednoczesnie
    '''

    def __init__(self, group_name, lines, time_delay=None, sensor=None):
        super().__init__(group_name, lines, time_delay, sensor)

    def action_off_on(self):
        '''
        akcja powoduje wylaczane lini jednoczesnie przez czas time_delay a nastepnie linie
        sa wlaczenie jednoczesnie
        '''
        super().action_off_on()
        log_item = LineLogItem('inclusive', self._group_name, 'off_on', self._execution_time, self._rainy)
        log_item.date_off = log_item.now()
        log_item.sensor = self.sensor.enabled
        log_item.mode = self.sensor.mode_name
        lines_off = []
        for line in self._lines:
            #self.ts_put_nowait(ThingSpeakItem(str(line.id), '1,0'))
            line.line_off()
            self.logger.info(func_name() + ' ' + self._group_name + ' line_id = ' + str(line.id) + ' ' + 'OFF')
            lines_off.append(line.id)

        log_item.lines_off = lines_off
        # entry[].append((self._group_name, line.id, 'OFF'))
        # self._execution_time.delay()  # opozniene po ktorym linie sa wlaczane

        if not self._rainy and not self.moisture:
            self._execution_time.delay()  # opozniene po ktorym linie sa wlaczane
            log_item.date_on = log_item.now()
            log_item.moisture = False
            lines_on = []
            for line in self._lines:
               #self.ts_put_nowait(ThingSpeakItem(str(line.id), '0,1'))
                line.line_on()
                self.logger.info(func_name() + ' ' + self._group_name + ' line_id = ' + str(line.id) + ' ' + 'ON')
                lines_on.append(line.id)

            log_item.lines_on = lines_on
        self.put_nowait(log_item.object2json())
        # self._event.set()
        with self.cv:
            self._is_executed = False
            self.cv.notify_all()

    def action_on_off(self):
        '''
        akcja powoduje wlaczenie lini jednoczesnie przez czas time_delay a nastepnie linie
        sa wylaczane jednoczesnie
        '''
        super().action_on_off()
        log_item = LineLogItem('inclusive', self._group_name, 'on_off', self._execution_time, self._rainy)
        log_item.sensor = self.sensor.enabled
        log_item.mode = self.sensor.mode_name
        if not self._rainy and not self.moisture:
            log_item.date_on = log_item.now()
            log_item.moisture = False
            lines_on = []
            for line in self._lines:
                #self.ts_put_nowait(ThingSpeakItem(str(line.id), '0,1'))
                line.line_on()
                self.logger.info(func_name() + ' ' + self._group_name + ' ' + 'line_id = ' + str(line.id) + ' ' + 'ON')
                lines_on.append(line.id)
            log_item.lines_on = lines_on
            self._execution_time.delay()  # opozniene po ktorym linie sa wylaczane

        log_item.date_off = log_item.now()
        lines_off = []
        for line in self._lines:
            #self.ts_put_nowait(ThingSpeakItem(str(line.id), '1,0'))
            line.line_off()
            lines_off.append(line.id)
            self.logger.info(func_name() + ' ' + self._group_name + ' ' + 'line_id = ' + str(line.id) + ' ' + 'OFF')

        log_item.lines_off = lines_off
        self.put_nowait(log_item.object2json())
        with self.cv:
            self._is_executed = False
            self.cv.notify_all()


class ExclusiveLinesAction(LinesAction):
    '''
      W InclusiveLinesAction akcjie wykonywane sa po kolei jedna po drugiej
    '''

    def __init__(self, group_name, lines, time_delay=None, sensor=None):
        super().__init__(group_name, lines, time_delay, sensor)

    def action_off_on(self):
        '''
        akcja powoduje iteracyje  wylaczenie przez czas time_delay a nastepnie wlaczenie linii
        '''
        super().action_off_on()
        log_item = LineLogItem('exclusive', self._group_name, 'off_on', self._execution_time, self._rainy)
        log_item.sensor = self.sensor.enabled
        log_item.mode = self.sensor.mode_name
        moisture = self.moisture
        log_item.moisture = moisture
        for line in self._lines:
            log_item.date_off = log_item.now()
            log_item.lines_off = [line.id]
            line.line_off()
            #self.ts_put_nowait(ThingSpeakItem(str(line.id), '1,0'))
            self.logger.info(func_name() + ' ' + self._group_name + ' ' + 'line_id = ' + str(line.id) + ' ' + 'OFF')
            if not self._rainy and not moisture:
                self._execution_time.delay()
                line.line_on()
                log_item.date_on = log_item.now()
                log_item.lines_on = [line.id]
                #self.ts_put_nowait(ThingSpeakItem(str(line.id), '0,1'))
                self.logger.info(func_name() + ' ' + self._group_name + ' ' + 'line_id = ' + str(line.id) + ' ' + 'ON')
            self.put_nowait(log_item.object2json())
        # self._event.set()
        with self.cv:
            self._is_executed = False
            self.cv.notify_all()

    def action_on_off(self):
        '''
        akcja powoduje iteracyje  wlaczenie przez czas time_delay a nastepnie wylaczenie linii
        '''
        super().action_on_off()
        log_item = LineLogItem('exclusive', self._group_name, 'on_off', self._execution_time, self._rainy)
        log_item.sensor = self.sensor.enabled
        log_item.mode = self.sensor.mode_name
        moisture = self.moisture
        log_item.moisture = moisture
        for line in self._lines:
            if not self._rainy and not moisture:
                line.line_on()
                #self.ts_put_nowait(ThingSpeakItem(str(line.id), '0,1'))

                log_item.date_on = log_item.now()
                log_item.lines_on = [line.id]
                self.logger.info(func_name() + ' ' + self._group_name + ' ' + 'line_id = ' + str(line.id) + ' ' + 'ON')
                self._execution_time.delay()
            line.line_off()
            #self.ts_put_nowait(ThingSpeakItem(str(line.id), '1,0'))

            log_item.date_off = log_item.now()
            log_item.lines_off = [line.id]
            self.logger.info(func_name() + ' ' + self._group_name + ' ' + 'line_id = ' + str(line.id) + ' ' + 'OFF')
            self.put_nowait(log_item.object2json())
        # self._event.set()
        with self.cv:
            self._is_executed = False
            self.cv.notify_all()
            # def action_interrupt(self):
            # super().action_interrupt()
