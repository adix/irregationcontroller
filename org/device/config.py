from org.device.pomp import Pomp

API_KEY = '2fafa7c7-bc4b-411d-8313-0c3468e9ada9'
HTTPS = False
THREADED = True
DEV_MODE = False
SSL_CONTEXT = None
if not DEV_MODE:
    CONTROLLER_LOG = '/var/log/icontroller.log'
    CONTROLLER_ERROR_LOG = '/var/log/icontroller_exception.log'
    HOST = 'localhost'
    DEBUG = False
    if HTTPS:
        SSL_CONTEXT = ('/etc/ssl/certs/server.crt', '/etc/ssl/private/server.key')
else:
    CONTROLLER_LOG = '../icontroller.log'
    CONTROLLER_ERROR_LOG = '../icontroller_exception.log'
    HOST = 'localhost'
    DEBUG = False  # True
    if HTTPS:
        SSL_CONTEXT = ('server.crt', 'server.key')

PORT = 5000
DB_HOST = 'localhost'
DB_PORT = 27017
DB_NAME = 'db_orange'
DB_COLL_REPORTS = 'reports'

ORANGEPI_ZERO = True

###Przekaznik###
RELAY_PORT_MAPPER = {'A': 'PA14', 'B': 'PA16', 'C': 'PA15', 'D': 'PA03'}

##Inicjalizacja Grup w executorze
DEFAULT_GROUP_COUNT = len(RELAY_PORT_MAPPER.keys())
###POMPA###
# POMP_ENABLE = False
POMP = Pomp(False)
POMP_PORT = 'PA10'

##Czujnik wilgotnosci##
SENSOR_ENABLE = False
SENSOR_PORT = 'PA06'
SENSOR_NO = 0
SENSOR_NC = 1
SENSOR_DEFAULT_STATE = SENSOR_NO

##USUNAC PO TESTACH
# HOST='192.168.0.20'
