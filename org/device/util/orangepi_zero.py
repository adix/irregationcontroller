import subprocess

from org.device.config import RELAY_PORT_MAPPER
from .logging import logger
try:
    from OPi import GPIO as gpio
except ImportError:
    pass

def setup():
    gpio.setwarnings(False)
    gpio.setmode(gpio.SUNXI)
    channels_out = ['PA10']
    channel_in = 'PA06'
    channels_to_clean = []
    for k, v in RELAY_PORT_MAPPER.items():
        channels_out.append(v)
    gpio.setup(channels_out, gpio.OUT)
    #print(channels_out)
    # if os.path.exists(sys_path.format(sunxi(channel_in))):
    #      gpio.cleanup(channel_in)
    gpio.setup(channel_in, gpio.IN)
    try:
        #Ustawienie pinu 7(PA06) pullup
        subprocess.check_call('gpio mode 7 up',shell=True)
    except subprocess.CalledProcessError as ex:
        logger.info(ex)