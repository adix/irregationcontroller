import inspect
import logging
from logging.handlers import TimedRotatingFileHandler

from org.device.config import CONTROLLER_LOG, CONTROLLER_ERROR_LOG

__author__ = 'anzietek'

# FORMAT = '%(levelname)s - %(message)s'
# logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)s %(message)s')  # datefmt='%Y-%m-%d %H:%M:%S'
# logger = logging.getLogger('org.device')
#

for handler in logging.root.handlers:
    handler.addFilter(logging.Filter('org.device'))

func_name = lambda: inspect.stack()[1][3]


class LoggerMixin:
    handler = TimedRotatingFileHandler(CONTROLLER_LOG, when='midnight', backupCount=2)

    @property
    def logger(self):
        lgr = logging.getLogger(self.__module__ + '.' + self.__class__.__name__)
        lgr.setLevel(logging.INFO)
        fmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(module)s : %(lineno)d - %(message)s')
        self.handler.setFormatter(fmt)
        lgr.addHandler(self.handler)
        return lgr

class __Logger(LoggerMixin):
    handler = TimedRotatingFileHandler(CONTROLLER_ERROR_LOG, when='midnight', backupCount=2)

logger = __Logger().logger
