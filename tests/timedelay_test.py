from threading import Thread, Event
from time import sleep
from unittest import TestCase
import time
import unittest

from org.device.json.object.timedelay import TimeDelay
from ddt import ddt, data


__author__ = 'anzietek'

skip_long_execution_test = True
skip_msg = 'skip test with long time execution'
def provider():
    data = {'seconds': 1, 'seconds': 5, 'seconds': 10, 'seconds': 33, 'seconds': 51, 'seconds': 57, 'seconds': 60,
                'minutes': 1, 'minutes': 5, 'minutes': 10, 'minutes': 20
    }
    return data


@ddt
class TimeDelayTest(TestCase):
    def setUp(self):
        pass



    @unittest.skipIf(skip_long_execution_test, skip_msg)
    @data(provider())
    def test_delay_exactly_execution_in_sec(self, value):
        td = TimeDelay(**value)
        seconds_begin = time.time()
        td.delay()
        seconds_end = time.time() - seconds_begin
        self.assertAlmostEqual(td.get_total_delay(), seconds_end, delta=1)


    # @data(1, 5, 10)
    # @unittest.skipIf(skip_long_execution_test, skip_msg)
    # def test_delay_exactly_execution_in_sec(self, minutes):
    #     td = TimeDelay(minutes=minutes)
    #     seconds_begin = time.time()
    #     td.delay()
    #     seconds_end = time.time() - seconds_begin
    #     self.assertAlmostEqual(td.get_total_delay(), seconds_end, places=1)

    @unittest.skip('')
    @data(provider())
    def test_delay_interrupt_in_sec(self, value):
        td = TimeDelay(**value)
        event = Event()

        def delay_task(td):
            td.delay()
            event.set()


        def interrupt_task(td):
            sleep(1)
            td.interrupt()

        delayTask = Thread(target=delay_task, args=(td,))
        interruptTask = Thread(target=interrupt_task, args=(td,))
        delayTask.start()
        interruptTask.start()
        td.interrupt()
        event.wait()
        self.assertFalse(td.is_delay_running(), 'TimeDelay shouldn\'t be running')

    def test_json(self):
        td=TimeDelay(hours=1,minutes=10,seconds=1)
        td.classmet()
        print(td)
        #json.loads()

if __name__ == '__main__':
    unittest.main()

