import os
import subprocess
from time import sleep
import OPi.GPIO as gpio
from OPi.pin_mappings import sunxi

out_list = ['PA14', 'PA16', 'PA15', 'PA03']
in_list = ['PA06']
RELAY_PORT_MAPPER = {'A': 'PA14', 'B': 'PA16', 'C': 'PA15', 'D': 'PA03'}


def setup_orangepi_zero():
    sys_path = '/sys/class/gpio/gpio{}'
    gpio.setmode(gpio.SUNXI)
    channels_out = ['PA10']
    channel_in = 'PA06'
    channels_to_clean = []
    for k, v in RELAY_PORT_MAPPER.items():
        path = sys_path.format(sunxi(v))
        # if os.path.exists(path):
        #     channels_to_clean.append(v)
        channels_out.append(v)
    # if len(channels_to_clean):
    #     gpio.cleanup(channels_to_clean)

    gpio.setup(channels_out, gpio.OUT)
    print(channels_out)
    # if os.path.exists(sys_path.format(sunxi(channel_in))):
    #      gpio.cleanup(channel_in)
    gpio.setup(channel_in, gpio.IN)
    try:
        subprocess.check_call('gpio mode 7 up',shell=True)
    except subprocess.CalledProcessError as ex:
        pass

def setup():
    out_list = ['PA14', 'PA16', 'PA15', 'PA03']
    in_list = ['PA06']
    gpio.setmode(gpio.SUNXI)
    gpio.setup(out_list, gpio.OUT)
    # gpio.setup(in_list,gpio.IN)


def main():
    i = 0
    while True:
        sleep(2)
        print(gpio.input('PA06'))

if __name__ == '__main__':
    setup_orangepi_zero()
    main()
