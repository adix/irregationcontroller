from org.device.json.object.group import Group
from org.device.json.object.timedelay import TimeDelay
from org.device.json.trigger import Interval

__author__ = 'anzietek'

dst_group = Group.auto_execution(id=0, trigger=Interval(seconds=2,timezone='utc'), action='off_on', type='exclusive',
                                         delay=TimeDelay(seconds=1),line_ids=[1,3])

print(dst_group.object2json())