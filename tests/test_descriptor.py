__author__ = 'anzietek'

class String:
    def __init__(self,name):
        self._name =name

    def __get__(self, instance, owner):
        print('self %s'%self)
        print('instance %s'%instance)
        print('owner %s'%owner)
        return  self._name

    def __set__(self, instance, value):
       self._name=value

class Foo:
    name=String('name')

    def __init__(self,value):
        self.name = value


def main():
    s=String('name')
    foo =Foo(None)
    print(foo.name)

if __name__ == '__main__':
    main()

