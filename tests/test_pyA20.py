from pyA20.gpio import gpio
from pyA20.gpio import port
pin_in=port.PA6
pin_out=port.PA14
def setup():

    gpio.init()
    gpio.setcfg(pin_out, gpio.OUTPUT)
    gpio.setcfg(pin_in, gpio.INPUT)
    gpio.pullup(pin_in, gpio.PULLUP)


def main():
    while True:
        gpio.output(pin_out,gpio.input(pin_in))


if __name__ == '__main__':
    setup
    main()