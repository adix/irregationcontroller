import pika


class TestQueue:


    def __init__(self, host='localhost', port=5672, queue_name='report_queue', user='', password=''):
        self.host = host
        self.port = port
        self.queue_name = queue_name
        self.user = user
        self.password = password
        self._init()

    def _init(self):
        self.credentials = None
        if self.user and self.password:
            self.credentials = pika.PlainCredentials(self.user, self.password)
        self.parameters = pika.ConnectionParameters(self.host, self.port, credentials=self.credentials)

    def generate(self, item):
        with pika.BlockingConnection(parameters=self.parameters if self.parameters else None) \
                as connection:
            channel = connection.channel()
            channel.queue_declare(queue=self.queue_name)
            channel.basic_publish(exchange='',
                                  routing_key=self.queue_name,
                                  body=item)
            # connection.close()


def main():
    q = TestQueue()
    for i in range(10):
        q.generate(str(i))


if __name__ == '__main__':
    main()