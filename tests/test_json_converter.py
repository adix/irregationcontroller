from org.device.json.object.group import Group
from org.device.json.object.timedelay import TimeDelay
from org.device.json.object.trigger import Interval

__author__ = 'anzietek'
import org.device.json.json_converter as js


class A(js.JsonMixin):
    _json_fields = [js.IntAttr('a'), js.StrAttr('b')]

    def __init__(self, name):
        self.name = name


class K:
    pass


class C(A, K):
    _json_fields = [js.StrAttr('c')]


class B(js.JsonMixin):
    _json_fields = [js.IntAttr('b'), js.IntListAttr('list_ids'), js.Attr('interval', _type=C)]

    def __init__(self, name):
        self.name = name


class X:
    def __init__(self):
        self._x = 'x'


class Y(X):
    def val(self):
        return getattr(self, '_x')


def main():
    _json = {
        '__class__': 'B',
        'b': 1,
        'list_ids': ['1', '2', '3'],
        'interval': {
            '__class__': 'C',
            '__bases__': [{'__class__': 'A', 'a': 1, 'b': 'b'}],
            'c': 'c'}}

    # td = TimeDelay(minutes=10)
    # _json = json.dumps(_json)
    # obj = B.json2object(_json)
    # _json = obj.object2json()
    # obj = B.json2object(_json)
    y = Y()
    interval = Interval(weeks=1, seconds=58, start_date='2015-10-11 12:10:10', timezone='utc')
    group = Group.auto_execution(id=0, trigger=interval, action='on_off', type='inclusive',
                                 delay=TimeDelay(seconds=1),line_ids=[1,2,3,4])
    _json = group.object2json()
    obj= Group.json2object(_json)
    print(obj.object2json())

    # js.register_type('INT2',int)
    # print(js.A)


if __name__ == '__main__':
    main()


