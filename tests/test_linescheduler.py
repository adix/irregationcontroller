import operator
from time import sleep

from apscheduler.schedulers.background import BackgroundScheduler
from org.device.json.object.group import Group
from org.device.json.object.mode import ManualMode, AutoMode
from org.device.core.scheduler import AppScheduler
from org.device.json.object.timedelay import TimeDelay
from org.device.json.object.trigger import Interval
import pytest


__author__ = 'anzietek'

clousure = lambda val, op: lambda group: op(val, group.id)
op_eq = operator.eq
op_in = operator.contains
op = lambda fn: getattr(operator, fn)

# logging.basicConfig(level=logging.DEBUG)
# logger = logging.getLogger('test')
@pytest.fixture(scope='function', autouse=False)
def lines_scheduler():
    groups = []
    # groups.append(group)
    for id in range(0, 6):
        group = Group.auto_execution(id=id, trigger=Interval(seconds=2, timezone='utc'), action='on_off',
                                     type='inclusive',
                                     delay=TimeDelay(seconds=1))
        groups.append(group)
    #lines_scheduler = LinesScheduler(groups=groups)


    return AppScheduler(scheduler=BackgroundScheduler(), groups=groups)


def test_lines_in_group(lines_scheduler):
    dst_group = Group.auto_execution(id=0, trigger=Interval(seconds=2, timezone='utc'), action='off_on',
                                     type='exclusive',
                                     delay=TimeDelay(seconds=1), line_ids=[1, 3])
    #print(id(lines_scheduler))
    lines_scheduler.start()
    sleep(5)
    lines_scheduler.modify_group(dst_group)
    groups = lines_scheduler.groups

    filtered = list(filter(clousure(0, op_eq), groups))
    assert len(filtered) == 1
    assert filtered[0].line_ids == [0, 1, 3]

    filtered = list(filter(clousure([1, 3], op_in), groups))
    assert len(filtered) == 2
    # assert filtered[0].line_ids == []
    # assert filtered[1].line_ids == []
    exec = filter(lambda obj: _assert(obj, [], op_eq, 'line_ids'), filtered)
    list(exec)
    filtered = list(filter(clousure([4, 5], op_in), groups))
    assert len(filtered) == 2
    exec = filter(lambda obj: _assert(obj, [obj.id], op_eq, 'line_ids'), filtered)
    list(exec)
    sleep(5)
    lines_scheduler.shutdown()


def _assert(obj, value, op, attr):
    _attr = getattr(obj, attr)
    assert op(_attr, value)
    return obj


def test_job_scheduler(lines_scheduler):
    dst_group = Group.auto_execution(id=0, trigger=Interval(seconds=2, timezone='utc'), action='off_on',
                                     type='exclusive',
                                     delay=TimeDelay(seconds=1), line_ids=[1, 3])
    #print(id(lines_scheduler))
    clazz = {
        'inclusive': 'InclusiveLinesAction',
        'exclusive': 'ExclusiveLinesAction'}
    lines_scheduler.start()
    sleep(5)
    lines_scheduler.modify_group(dst_group)
    lines_scheduler.modify_group(dst_group)
    groups = lines_scheduler.groups
    sleep(5)
    for group in groups:
        if isinstance(group.mode, AutoMode):
            job = lines_scheduler._scheduler.get_job(job_id=group.name)
            job_action = job.func.__name__
            group_action = 'action_%s' % group.mode.action
            assert job_action == group_action
            job_class_name = job.func.__self__.__class__.__name__
            group_class_name = clazz[group.type]
            assert job_class_name == group_class_name
            assert job.trigger == group.mode.trigger.name
            assert job.func.__self__._execution_time == group.delay

    jobs = lines_scheduler._scheduler.get_jobs()
    lines_scheduler.shutdown()


def test_json_job_scheduler(lines_scheduler):
    dst_group = Group.auto_execution(id=0, trigger=Interval(seconds=2, timezone='utc'), action='off_on',
                                     type='exclusive',
                                     delay=TimeDelay(seconds=1), line_ids=[1, 3])
    #print(id(lines_scheduler))
    clazz = {
        'inclusive': 'InclusiveLinesAction',
        'exclusive': 'ExclusiveLinesAction'}
    lines_scheduler.start()
    sleep(5)
    lines_scheduler.modify_group(dst_group)
    sleep(5)
    lines_scheduler.modify_group(dst_group)
    groups = lines_scheduler.groups
    sleep(5)
    lines_scheduler.shutdown()
    _json = lines_scheduler.object2json()
    newsched = AppScheduler.json2object(_json)
    newsched.start()
    sleep(5)
    newsched.shutdown()
    _newjson = newsched.object2json()
    print(_newjson)
    assert _json == _newjson


def test_manual_job_scheduler(lines_scheduler):
    dst_group = Group.manual_execution(id=0, action='off_on',
                                       type='exclusive',
                                       delay=TimeDelay(seconds=1), line_ids=[1, 3])
    lines_scheduler.start()
    sleep(5)
    lines_scheduler.modify_group(dst_group)
    sleep(5)
    lines_scheduler.modify_group(dst_group)
    groups = lines_scheduler.groups
    for group in groups:
        if group.id == 0:
            job = lines_scheduler._scheduler.get_job(job_id=group.name)
            assert  isinstance(group.mode, ManualMode) == True
            assert job == None
            assert group.id == 0
            assert group.name == 'group_0'
            assert group.line_ids == [0, 1, 3]
    lines_scheduler.shutdown()