from unittest import TestCase, main

from org.device.core.relay import Relay


__author__ = 'anzietek'


class RelayTest(TestCase):
    def setUp(self):
        self.relay = Relay(port_id='A')


    def test_relay_on(self):
        self.relay.relay_on()
        self.assertEqual(self.relay.state, self.relay._ON, 'state should be _ON ')

    def test_relay_off(self):
        self.relay.relay_off()
        self.assertEqual(self.relay.state, self.relay._OFF, 'state should be _OFF')

    def test_toggle(self):
        state = self.relay.state
        self.relay.toggle()
        new_state = self.relay.state
        self.assertNotEqual(state, new_state, 'state should be  different')

    def test_repr(self):
        self.assertEqual("<Relay('A')>", repr(self.relay))


if __name__ == '__main__':
    main()