from time import sleep
from unittest import TestCase, main, skip

from org.device.json.object.group import Group
from org.device.core.line import LinesManager
from org.device.core.scheduler import AppScheduler
from org.device.json.object.timedelay import TimeDelay
from org.device.json.object.trigger import Interval


__author__ = 'anzietek'


class LinesManagerTest(TestCase):
    def setUp(self):
        self.groups_number = 6
        self.line_manager = LinesManager(self.groups_number)

    @skip('')
    def test_init_condition(self):
        groups = self.line_manager.groups
        self.assertEqual(self.groups_number, len(groups.keys()))
        for _, (group_name, lines) in enumerate(groups.items()):
            self.assertEqual(group_name, 'group_%s' % lines[0].id)

    @skip('')
    def test_move_line_to_group(self):
        self.line_manager.move_line_to_group(line_id=0, group_name=5)
        self.line_manager.move_line_to_group(line_id=4, group_name=1)
        self.line_manager.move_line_to_group(line_id=3, group_name=1)

        self.assertEqual(0, len(self.line_manager.groups['group_0']))

        self.assertEqual(0, len(self.line_manager.groups['group_3']))

        self.assertEqual(0, len(self.line_manager.groups['group_4']))

        self.assertEqual(3, len(self.line_manager.groups['group_1']))

    @skip('')
    def test_restore_to_defaults(self):
        default_groups = dict(self.line_manager.groups)
        self.line_manager.move_line_to_group(line_id=0, group_name=5)
        self.line_manager.move_line_to_group(line_id=4, group_name=1)
        self.line_manager.move_line_to_group(line_id=3, group_name=1)

        self.line_manager.restore_to_default_groups()
        restored = self.line_manager.groups
        self.assertNotEqual(id(default_groups), id(restored))
        for _, (group_name, lines) in enumerate(restored.items()):
            self.assertEqual(1, len(lines))
            self.assertEqual(group_name, 'group_%s' % lines[0].id)

    def test_line_scheduler(self):
        groups = []
        dst_group = Group.auto_execution(id=0, trigger=Interval(seconds=2,timezone='utc'), action='off_on', type='exclusive',
                                         delay=TimeDelay(seconds=1),line_ids=[1,3])
        #groups.append(group)
        for id in range(0,6):
            group = Group.auto_execution(id=id, trigger=Interval(seconds=2,timezone='utc'), action='on_off', type='inclusive',
                                         delay=TimeDelay(seconds=1))
            groups.append(group)
        line_scheduler = AppScheduler(groups=groups)
        line_scheduler.start()
        sleep(5)
        line_scheduler.modify_group(dst_group)
        sleep(5)
        line_scheduler.shutdown()#(wait=False)
        # _json =line_scheduler.object2json()
        # obj=LinesScheduler.json2object(_json)
        # print(_json)
        # obj.start()
        # obj.shutdown()
        print(line_scheduler.object2json())


if __name__ == '__main__':
    main()